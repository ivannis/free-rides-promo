# Free rides promo

CompanyX want to give out promo codes worth x amount during events so people can get free rides to and from the event. The flaw with that is people can use the promo codes without going for the event.

## Task

Implement a promo code api with the following features:

- Generation of new promo codes for events
- The promo code is worth a specific amount of ride
- The promo code can expire
- Can be deactivated
- Return active promo codes
- Return all promo codes
- Only valid when user’s pickup or destination is within x radius of the event venue
- The promo code radius should be configurable
- To test the validity of the promo code, expose an endpoint that accept origin, destination, the promo code. The api should return the promo code details and a polyline using the destination and origin if promo code is valid and an error otherwise.

## What's inside?

The project it was divided into 2 applications:

**Backend**: A backend API application based on Symfony + Cubiche that contains the resolution of all the features requested.

**Frontend**: A frontend application based on React + Redux + Redux Saga + NextJS to play with the API.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

Clone the repository

```bash
git clone https://gitlab.com/ivannis/free-rides-promo.git
```

### Installing

- Install the [backend application](https://gitlab.com/ivannis/free-rides-promo/blob/master/backend/README.md)
- Install the [frontend application](https://gitlab.com/ivannis/free-rides-promo/blob/master/frontend/README.md)

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)