module.exports = {
    'process.env.api': {
        url: process.env.API_URL || 'http://api.freerides.local/graphql'
    },
    'process.env.google_map': {
        key: 'AIzaSyBvY8oYPSOG9eOQHTo-vmr4fd5qqhupf5k',
        zoom: 24
    },
    'process.env.defaultLanguage': 'en_US'
};