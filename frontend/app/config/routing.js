const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes
    .add('home', '/', 'index')
    .add('user_login', '/login', 'login')
    .add('user_register', '/register', 'register')
    .add('user_reset_password_request', '/reset-password-request', 'resetPasswordRequest')
    .add('user_reset_password', '/reset-password/:token', 'resetPassword')
    .add('dataset_list', '/datasets', 'dataset/index')
    .add('log_list', '/logs', 'log/index')
;
