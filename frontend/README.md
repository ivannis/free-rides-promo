# FreeRides Frontend

The project contains a frontend application with React + Redux + Redux Saga to play with the backend API.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

Open a terminal and follow the [instrucctions](https://gitlab.com/ivannis/free-rides-promo/blob/master/backend/README.md) to have the backend project running.
Open another terminal and install the project.

### Installing

```bash
# in your host machine (Not inside the container)
cd /directory/with/the/frontend
npm install
npm run build
```

### Running the application

**Important Note:**
```code
To be able to run the frontend application the API application must be running
```

```bash
npm run start
```

## Tests

### Running all the unit tests suite

```bash
npm run test
```

### Running all the tests coverage

```bash
npm run coverage
```

## Built With

* [NextJS](https://nextjs.org/) - A server-rendered framework for React apps
* [React](https://reactjs.org/) + [Redux](https://redux.js.org/) + [Redux Saga](https://redux-saga.js.org/)

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)