import Document, {Head, Main, NextScript} from 'next/document'

// The document (which is SSR-only) needs to be customized to expose the locale
// data for the user's locale for React Intl to work in the browser.
export default class IntlDocument extends Document {
    static async getInitialProps (context) {
        const props = await super.getInitialProps(context)
        const { req: { locales, localeDataScripts } } = context

        const features = [];
        const scripts = localeDataScripts || []

        if (locales !== undefined) {
            locales.forEach(function(locale){
                features.push('Intl.~locale.' + locale)
            })
        }

        return {
            ...props,
            features,
            scripts
        }
    }

    render () {
        // Polyfill Intl API for older browsers
        const polyfill = `https://cdn.polyfill.io/v2/polyfill.min.js?features=${this.props.features.join(',')}`

        return (
            <html>
                <Head>
                    <link rel="stylesheet" href="/_next/static/style.css" />
                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"></link>
                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css"></link>
                    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></link>
                    <link rel="stylesheet" type="text/css" href="//marslan390.github.io/BeautifyMarker/leaflet-beautify-marker-icon.css"></link>                                  
                </Head>
                <body>
                    <Main />
                    <script src={polyfill} />
                    <script
                        dangerouslySetInnerHTML={{
                            __html: this.props.scripts.join(';')
                        }}
                    />
                    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoq4_-BeKtYRIs-3FjJL721G1eP5DaU0g&libraries=places"></script>
                    <NextScript />
                </body>
            </html>
        )
    }
}