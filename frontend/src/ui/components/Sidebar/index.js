import React from "react";
import { Layout } from 'antd';
import Link from '../Link'
import Scrollbar from '../Scrollbar'
import NavigatorForm from '../Map/navigatorForm'
import PromoCodeList from '../PromoCode/PromoCodeList'

const { Sider } = Layout;
const styles = `       
    .logo-container {
        position: relative;
        height: 64px;
        background: #002140;
    }
    
    .logo {
        cursor: pointer;
        left: 24px;
        top: 10px;
        background: #7078FF;
        width: 36px;
        height: 36px;
        display: block;
        position: absolute;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        border-bottom-left-radius: 20px;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    .logo::before{
        width: 20px;
        height: 20px;
        display:block;
        border:5px solid #F7F5F2;
        content:"";
        position:absolute;
        border-radius:14px;
        top:8px;
        left:8px;
    }
    .brand-name {
        cursor: pointer;
        font-weight: bold;
        color: #fff;
        font-size: 20px;
        position: absolute;
        left: 70px;
        top: 18px;
        display: block;
    }    
`
const globalStyles = `
    .ant-layout-sider-dark {
        background: #002140;
    }
    .ant-layout-sider-trigger {
      transition: color .3s;
    }    
    
    .ant-layout-sider-trigger:hover {
        color: #1890ff;
    }
`
export default class Sidebar extends React.Component {
    state = {
        collapsed: false,
    };
    
    render() {

        return (
            <Sider                
                className="sidebar"
                width="480px"
                collapsedWidth="70px"     
            >
                <style jsx="true">{styles}</style>
                <style global="true" jsx="true">{globalStyles}</style>

                <Link route='home'>
                    <div className="logo-container">
                        <div className="logo"></div>
                        <div className="brand-name">FreeRides</div>
                    </div>
                </Link>
                <div style={{background: '#fff'}}>    
                    <NavigatorForm/>
                    <div style={{width: '100%', height: 'calc(100vh - 415px)'}}>    
                        <Scrollbar>                        
                            <PromoCodeList/>
                        </Scrollbar>                    
                    </div>
                </div>                
            </Sider>
        );
    }
}