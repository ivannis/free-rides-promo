import Layout from './BasicLayout'
import BlankLayout from './BlankLayout'
import FluidLayout from './FluidLayout'

export { FluidLayout }
export { BlankLayout }
export default Layout