import React from "react";
import { Layout } from 'antd';
import Sidebar from '../Sidebar'
import Header from '../Header'
import Footer from '../Footer'

const { Content } = Layout;
const styles = `   
    .basicLayout {
        min-height: 100vh;
        flex-direction: row;
    }    
`

export default class BasicLayout extends React.Component {
    render() {
        return (
            <Layout className="basicLayout">
                <style jsx="true">{styles}</style>                
                <Sidebar />

                <Layout>
                    <Header />

                    <Content className="content">
                        {this.props.children}
                    </Content>

                    <Footer />
                </Layout>
            </Layout>
        );
    }
}