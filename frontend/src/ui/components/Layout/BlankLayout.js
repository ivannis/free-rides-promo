import { Layout } from 'antd';
import React from "react";
import Footer from '../Footer'

const { Header, Content } = Layout;
const styles = `   
    .content {
        background: #fff;
        min-height: 280px
    }  
`
export default class BlankLayout extends React.Component {
    render() {
        return (
            <Layout>
                <style jsx="true">{styles}</style>                
                <Header />

                <Content className="content">
                    {this.props.children}
                </Content>

                <Footer />
            </Layout>
        );
    }
}