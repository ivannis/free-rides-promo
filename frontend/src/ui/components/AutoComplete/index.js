import React from 'react';
import GooglePlaceAutoComplete from "./GooglePlaceAutocomplete";

class AutoComplete extends React.Component {
    constructor(props) {
        super(props);

        this.placeAutocomplete = React.createRef();
        const value = props.value || {};

        this.state = {
            address: value.address || null,
            latitude: value.latitude || null,
            longitude: value.longitude || null
        };
    }

    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            if (value === undefined || (value.address === null)) {
                if (this.placeAutocomplete.current) {
                    this.placeAutocomplete.current.reset();
                }    

                this.setState({
                    address: null,
                    latitude: null,
                    longitude: null
                })
            } else {
                this.setState(value)
            }
        }
    }

    updateState(newState, callback = ()=>{}) {
        if (!('value' in this.props)) {
            this.setState(newState, callback);
        }

        this.triggerChange(newState);
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }

    handlePlaceSelected = (prediction) => {     
        this.updateState({
            address: prediction.label,
            latitude: prediction.geometry.location.lat(),
            longitude: prediction.geometry.location.lng()
        });

        const onPlaceSelected = this.props.onPlaceSelected;
        if (onPlaceSelected) {
            onPlaceSelected(prediction);
        }
    }

    handleOnChange = ({ label }) => {
        this.updateState({
            address: label
        });
    }

    render() {
        return (
            <div>
                <GooglePlaceAutoComplete
                    ref={this.placeAutocomplete}
                    prefix={this.props.prefix}                                                     
                    componentRestrictions={this.props.restrictions}
                    placeholder={this.props.placeholder}
                    size={this.props.size}
                    style={this.props.style}     
                    value={this.props.value}
                    onPlaceSelected={this.handlePlaceSelected}
                    onChange={this.handleOnChange}       
                />
            </div>
        );
    }    
}

export default AutoComplete;