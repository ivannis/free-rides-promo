import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import classNames from 'classnames'

const Option = Select.Option;

const styles = `    
    .ant-input-affix-wrapper.with-prefix .ant-input-prefix {
        z-index: 1;
    }    
    .ant-input-affix-wrapper.with-prefix .ant-select-selection {
        padding-left: 25px;
    }
`

export default class GooglePlaceAutocomplete extends React.Component {
    static propTypes = {
        prefix: PropTypes.node,
        onPlaceSelected: PropTypes.func,
        onChange: PropTypes.func,
        types: PropTypes.array,
        componentRestrictions: PropTypes.object,
        bounds: PropTypes.object
    }

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            predictions: [],
            placeId: undefined, 
            label: undefined, 
            place: undefined,
            ready: !props.googleCallbackName,
        };
    }
    
    reset = () => {  
        this.setState({ 
            loading: false,
            predictions: [],
            placeId: undefined, 
            label: undefined, 
            place: undefined,
            ready: true
        })
    }

    onBlur = () => {  
        const onComponentChange = this.props.onChange;
        if (onComponentChange) {            
            onComponentChange({ label: this.state.label });
        }
    }

    onSearch = (query) => {  
        const onComponentChange = this.props.onChange;
        if (onComponentChange) {
            onComponentChange({ label: query });
        }

        const { types = [], componentRestrictions, bounds } = this.props;
        const config = { types, bounds };

        if (componentRestrictions) {
            config.componentRestrictions = componentRestrictions;
        }
        
        this.setState({ loading: true }) 
        this.service.getPlacePredictions({ input: query, ...config }, (predictions, status) => {
            if (status === 'OK' && predictions && predictions.length > 0) {
                this.setState({ 
                    predictions, 
                    placeId: undefined, 
                    label: undefined, 
                    place: undefined,
                    loading: false 
                })
            } else {
                this.setState({ 
                    predictions: [], 
                    placeId: undefined, 
                    label: undefined, 
                    place: undefined,
                    loading: false
                })
            }
        });
    }
    
    onChange = ({ key, label }, callback = () => {}) => {     
        this.setState({ placeId: key, label }, () => {
            this.findPlace(key, (place, status) => {
                const onPlaceSelected = this.props.onPlaceSelected;
                if (onPlaceSelected) {
                    onPlaceSelected(this.state.place);
                }

                if (callback instanceof Function) {
                    callback(place, status);
                }                
            });  
        })    
    }

    findPlace = (placeId, callback = () => {}) => {
        let { label } = this.state;        
        this.setState({ loading: true })

        this.placeService.getDetails({ placeId }, (place, status) => {            
            if (status === 'OK') {
                label = label || place.formatted_address

                this.setState({ 
                    place: {
                        ...place,
                        label
                    },
                    loading: false
                }, () => {
                    callback(this.state.place, status);
                })
            } else {
                this.setState({ 
                    place: undefined,
                    loading: false
                })

                callback(place, status);
            } 
        });
    }
    
    componentDidMount() {
        const { googleCallbackName } = this.props;
        if (googleCallbackName) {
            if (!window.google) {
                window[googleCallbackName] = this.init;
            } else {
                this.init();
            }
        } else {
            this.init();
        }

        if (this.props.value) {
            this.onChange({ key: this.props.value }, (place, status) => {
                if (status === 'OK') {
                    this.setState({ 
                        predictions: [{
                            ...place,
                            description: place.label
                        }]
                    })
                }
            });            
        }
    }
    
    componentWillUnmount() {
        const { googleCallbackName } = this.props;
        if (googleCallbackName && window[googleCallbackName]) {
            delete window[googleCallbackName];
        }
    }

    init = () => {
        if (!window.google) {
            throw new Error(
                'Google Maps JavaScript API library must be loaded.'
            );
        }

        if (!window.google.maps.places) {
            throw new Error(
                'Google Maps Places library must be loaded. Please add `libraries=places` to the src URL.'
            );
        }

        this.service = new window.google.maps.places.AutocompleteService();
        this.placeService = new google.maps.places.PlacesService(this.refs.div);            
        this.setState(state => {
            if (state.ready) {
                return null;
            } else {
                return { ready: true };
            }
        });       
    };

    render() {
        const options = this.state.predictions.map(prediction => <Option key={prediction.place_id} >{prediction.description}</Option>);
        const className = classNames('ant-input-affix-wrapper', {
            'with-prefix': this.props.prefix ? true : false,
        });

        return (
            <div className={className}>       
                <style jsx="true">{styles}</style>         
                <span className="ant-input-prefix">
                    {this.props.prefix || ''}
                </span>
                <Select
                    showSearch
                    value={this.state.placeId && this.state.loading === false ? { key: this.state.placeId } : undefined}
                    placeholder={this.props.placeholder}
                    size={this.props.size}
                    style={this.props.style}
                    defaultActiveFirstOption={false}
                    showArrow={false}
                    filterOption={false}
                    onSearch={this.onSearch}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    notFoundContent={null}
                    labelInValue={true}
                >
                    {options}
                </Select>     
                <div ref="div"></div>
            </div>                   
        );
    }
}
