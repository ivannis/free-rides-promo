import React from "react";
import { Alert } from 'antd';

const isObject = function(obj) {
    return obj === Object(obj);
};
const ObjectToArray = (errors = {}) => {    
    const messages = []    
    for (var key in errors) {
        messages.push(errors[key]);
    }

    return messages;
}

const ErrorAlert = ({errors = []}) => {   
    if (isObject(errors)) {
        errors = ObjectToArray(errors);
    }

    return (       
        errors.map(function(error, i) {
            return <Alert showIcon={false} type="error" message={ error } banner key={i}/>
        })                
    )
}

export default ErrorAlert

