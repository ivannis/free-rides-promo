import {array, object} from "prop-types";
import {Path, withLeaflet} from "react-leaflet";
import {antPath} from "leaflet-ant-path";

class AntPathComponent extends Path {
    static defaultProps = {};

    static propTypes = {
        positions: array.isRequired,
        options: object
    };

    createLeafletElement(props) {
        const {positions, options} = props;
        return antPath(positions, options);
    }

    updateLeafletElement(fromProps, toProps) {
        if (toProps.positions !== fromProps.positions) {
            this.leafletElement.setLatLngs(toProps.positions)
        }
        this.leafletElement.setStyle({...fromProps.options, ...toProps.options});
    }
}

export default withLeaflet(AntPathComponent)