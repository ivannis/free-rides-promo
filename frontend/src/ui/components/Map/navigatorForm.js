import React, { Component } from 'react'
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl'
import { Form, Icon, Input, Button, Spin } from 'antd';
import { IntlContext } from "../../../infrastructure/with/withIntlContext";
import ErrorAlert from "../Alert/ErrorAlert"
import AutoComplete from '../AutoComplete';
import App from "../../../application";

const FormItem = Form.Item;
const styles = `       
    .start-navigation-container {
        width: 400px;
        background: #fff;
    }

    .start-navigation {
        width: 48%;
        margin-right: 4%;
    }
    
    .stop-navigation {
        width: 48%;
    }
    .navigator-form {        
        background: #fff;
        padding: 0px 40px 30px 40px;
        box-shadow: 0 3px 5px 0 rgba(23, 35, 44, 0.24)!important;
        margin-bottom: 5px;
    }    
    .form-error-container {
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        z-index: 999;
    }  
    .navitagor-title {
        text-align: center;
        margin-bottom: 10px;
    }
`
const globalStyles = `
    .ant-form-item {
        margin-bottom: 10px;
    }
    .ant-input-affix-wrapper .ant-select-selection,
    .ant-input-affix-wrapper .ant-input {
        border-radius: 2px!important;
        box-shadow: 0 1px 5px 0 rgba(23, 35, 44, 0.24)!important;
        font-size: 14px;
    }    
    .ant-input-affix-wrapper .ant-input:not(:first-child) {
        padding-left: 35px;
    }
    .has-error .ant-select-selection,
    .has-error .ant-select-open .ant-select-selection, .has-error .ant-select-focused .ant-select-selection,
    .has-error .ant-input:focus, 
    .has-error .ant-input:not([disabled]):hover, 
    .has-error .ant-input, .has-error .ant-input:hover {
        border-color: #d9d9d9;
    }    
    .has-error .ant-form-explain, .has-error .ant-form-split {
        color: #FF6151;
        position: absolute;
        top: 12px;
        right: 35px;
        background: #fff
    }
    .ant-alert.ant-alert-no-icon {
        padding: 8px 15px;
        min-height: 60px;
        text-align: center;
    }
    .show-help-leave {
        position: absolute;
        top: 12px;
        right: 35px;
        background: #fff
    }
    .show-help-enter.show-help-enter-active,
    .show-help-appear.show-help-appear-active {
        position: absolute;
        top: 12px;
        right: 35px;
        background: #fff
    }
`

const ContextButtom = ({ readyNavigation, onStart, onStop, onNavigation }) => {    
    if (readyNavigation) {
        return <div className="start-navigation-container">
            <Button type="primary" className="start-navigation" onClick={onStart} disabled={onNavigation} block>
                <FormattedMessage id='button.navigation.start' defaultMessage='Start navigation' />
            </Button>  
            <Button type="danger" className="stop-navigation" onClick={onStop} disabled={!onNavigation} block>
                <FormattedMessage id='button.navigation.stop' defaultMessage='Stop navigation' />
            </Button> 
        </div>
    }

    return <Button type="primary" className="start" htmlType="submit" block>
        <FormattedMessage id='button.directions' defaultMessage='Directions' />
    </Button>
}

class NavigatorForm extends Component {    
    state = {
        code: undefined,
        origin: undefined,
        destination: undefined,
        readyNavigation: false,
        onNavigation: false
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.promoCode !== this.props.promoCode && nextProps.promoCode.polyline !== undefined) {
            this.setState({ readyNavigation: true })
        }
    }

    onStartNavigation = () => {
        this.setState({onNavigation: true})
        this.props.startNavigation();
    }

    onStopNavigation = () => {
        this.setState({onNavigation: false, readyNavigation: false })

        this.props.stopNavigation();
        this.props.use(this.state.code, this.state.origin, this.state.destination);
        this.props.form.resetFields();
    }

    handleSubmit = (e) => {
        e.preventDefault();        
        this.setState({clientErrors: {}});

        this.props.form.validateFields((error, values) => {                
            if (!error) {   
                this.setState({
                    code: values.code,
                    origin: values.origin.address,
                    destination: values.destination.address
                })

                this.props.validate(values.code, values.origin.address, values.destination.address);                
            }
        });
    }

    checkAddress = (rule, value, callback) => {        
        if (value !== undefined && value.address !== undefined) {
          callback();

          return;
        }

        callback(rule.message);
    }

  render() {
    const { errors, isLoading } = this.props;
    const { getFieldDecorator } = this.props.form;
    
    return (
        <IntlContext.Consumer>
        {intl => (
            <div>
                <style jsx="true">{styles}</style>
                <style global="true" jsx="true">{globalStyles}</style>            
                <Spin spinning={isLoading}>
                    <Form onSubmit={this.handleSubmit} className="navigator-form">
                        <div className="form-error-container">
                            <ErrorAlert errors={errors}/>
                        </div>
                        <FormItem>
                            <h1 className="navitagor-title">
                                <FormattedMessage id='navigator.title' defaultMessage='Set location' />
                            </h1>
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('code', {
                                rules: [
                                    { 
                                        required: true, 
                                        message: intl.formatMessage({ id: 'form.error.code', defaultMessage: "Please input your promo code!" })
                                    }
                                ]
                            })(
                                <Input 
                                    prefix={<Icon type="tags" 
                                    style={{ fontSize: '16px', color: '#A9B0C7' }} />} 
                                    placeholder={intl.formatMessage({ id: 'form.placeholder.code', defaultMessage: "Promo Code" })} 
                                    size="large" 
                                />        
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('origin', {
                                rules: [
                                    { 
                                        validator: this.checkAddress, 
                                        message: intl.formatMessage({ id: 'form.error.origin', defaultMessage: "Please input your origin!" })
                                    }
                                ]
                            })(
                                <AutoComplete 
                                    prefix={<i className={"fa fa-dot-circle-o"} style={{ fontSize: '16px', color: '#A9B0C7' }}></i>}
                                    placeholder={intl.formatMessage({ id: 'form.placeholder.origin', defaultMessage: "Origin" })} 
                                    size="large"
                                />
                            )}                        
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('destination', {
                                rules: [
                                    { 
                                        validator: this.checkAddress, 
                                        message: intl.formatMessage({ id: 'form.error.destination', defaultMessage: "Please input your destination!" })
                                    }
                                ]
                            })(
                                <AutoComplete 
                                    prefix={<Icon type="environment" style={{ fontSize: '16px', color: '#A9B0C7' }} />}
                                    placeholder={intl.formatMessage({ id: 'form.placeholder.destination', defaultMessage: "Destination" })} 
                                    size="large"
                                />
                            )}                        
                        </FormItem>                    
                        <FormItem style={{ marginBottom: '0px' }}>    
                            <ContextButtom 
                                readyNavigation={this.state.readyNavigation} 
                                onNavigation={this.state.onNavigation} 
                                onStart={this.onStartNavigation} 
                                onStop={this.onStopNavigation} 
                            />                            
                        </FormItem>
                    </Form>
                </Spin>                          
            </div>      
        )}
        </IntlContext.Consumer>    
    )
  }
}

const actions = App.actions.promoCode;
const selectors = App.selectors.promoCode;

const mapStateToProps = (state) => ({
    promoCode: selectors.current(state),
    isLoading: selectors.isLoading(state),
    errors: selectors.errors(state)
});

const mapDispatchToProps = (dispatch) => ({    
    validate: (promoCode, origin, destination) => dispatch(actions.validate(promoCode, origin, destination)),
    use: (promoCode, origin, destination) => dispatch(actions.use(promoCode, origin, destination)),
    startNavigation: () => dispatch(App.actions.startNavigation()),
    stopNavigation: () => dispatch(App.actions.stopNavigation())
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(NavigatorForm));