import React, { Component } from 'react'
import { connect } from "react-redux";
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import Leaflet from 'leaflet'
import 'beautifymarker/leaflet-beautify-marker-icon.js'
import App from "../../../application";
import AntPath from "./AntPath";
import polyline from '@mapbox/polyline';
import styles from "./styles";

Leaflet.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/images/'

const customIcon = L.BeautifyIcon.icon({
    icon: "circle",
    iconShape: 'marker',
    borderColor: '#7078FF',
    textColor: '#7078FF'
})

const originIcon = L.BeautifyIcon.icon({
    iconShape: 'doughnut',
    borderWidth: 5,
    borderColor: '#7078FF',
    textColor: '#7078FF',
    customClasses: 'marker-origin',
    html: '<div class="pulse"></div>'
})

const destinationIcon = L.BeautifyIcon.icon({
    icon: "motorcycle",
    iconShape: 'marker',
    borderColor: '#7078FF',
    textColor: '#7078FF'
})

const PopupMarker = ({ children, position }) => (
    <Marker position={position} icon={customIcon}>
        <Popup>{children}</Popup>
    </Marker>
)

const MarkersList = ({ markers }) => {
    const items = markers.map(({ key, ...props }) => (
        <PopupMarker key={key} {...props} />
    ))
    return <div style={{ display: 'none' }}>{items}</div>
}

const OriginMarker = ({ children, position }) => (
    <Marker position={position} icon={originIcon}>
        {children}
    </Marker>
)

const DestinationMarker = ({ children, position }) => (
    <Marker position={position} icon={destinationIcon}>
        {children}
    </Marker>
)

const Directions = ({ waypoints, options, onNavigation }) => {
    if (waypoints !== undefined) {        
        return <div>
            <OriginMarker position={waypoints[0]} />
            <DestinationMarker position={waypoints[waypoints.length-1]} />                
            <AntPath positions={waypoints} options={{...options, paused: !onNavigation }} />
        </div>
    }

    return '';
}

class VenuesMap extends Component {
    state = {
        lat: 41.3851,
        lng: 2.1734,
        zoom: 13,
        waypoints: undefined,
        onNavigation: false
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.promoCode !== this.props.promoCode && nextProps.promoCode.polyline !== undefined) {
            this.setState({ 
                waypoints: polyline.decode(nextProps.promoCode.polyline),
                lat: nextProps.promoCode.coordinate.latitude,
                lng: nextProps.promoCode.coordinate.longitude,
            })
        }

        if (nextProps.onNavigation !== this.props.onNavigation && nextProps.onNavigation == false) {
            this.setState({ waypoints: undefined })
        }
    }

    render() {
        const { venues, onNavigation } = this.props;
        const center = [this.state.lat, this.state.lng]
        const antPathOptions = {
            "delay": 660,
            "dashArray": [80,35],
            "weight": 5,
            "color": "#0000FF",
            "pulseColor": "#FFFFFF",
            "paused": true,
            "reverse": false
        }

        const markers = venues.map(function(venue) {
            return { 
                'key': venue.id, 
                'position': [venue.coordinate.latitude, venue.coordinate.longitude], 
                'children': venue.name 
            };          
        });
        
        return (
            <div>
                <style jsx="true">{styles}</style>
                <Map center={center} zoom={this.state.zoom} ref={map => this.map = map}>                    
                    <TileLayer
                        attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                        url="https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png"                
                    />
                    <MarkersList markers={markers} />
                    <Directions waypoints={this.state.waypoints} options={antPathOptions} onNavigation={onNavigation} />            
                </Map>                
            </div>            
        )
    }
}

const actions = App.actions.venue.list;
const selectors = App.selectors.venue.list;
const promoCodeSelectors = App.selectors.promoCode;

const mapStateToProps = (state) => ({
    venues: selectors.all(state),
    isLoading: selectors.isLoading(state),
    onNavigation: App.selectors.onNavigation(state),
    promoCode: promoCodeSelectors.current(state)
});

const mapDispatchToProps = (dispatch) => ({    
    use: (promoCode, origin, destination) => dispatch(actions.use(promoCode, origin, destination))
});

export default connect(mapStateToProps, mapDispatchToProps)(VenuesMap)