import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl'
import { List, Avatar } from 'antd';
import classNames from 'classnames'
import App from "../../../application";

const styles = `   
    .promo-code {
        font-weight: 600;
        color: #7078FF;
        font-family: Avenir Next,Calibri,Helvetica,Roboto,sans-serif;
        font-size: 15px;
    }
    .promo-code.sold-out {        
        color: #FF6151;
    }
    .promo-code.disabled {
        color: #cccccc;
    }
    .promo-code-avatar {
        color: fff;
        background: #7078FF;
    }
    .promo-code-avatar.sold-out {
        background: #FF6151;
    }
    .promo-code-avatar.disabled {
        background: #cccccc;
    }
    .promo-uses {        
        font-size: 12px;
    }
    .sold-out-strip {
        top: 25px;
        left: -4.5em;
        color: #fff;
        display: block;
        position: absolute;
        text-align: center;
        text-decoration: none;
        letter-spacing: .06em;
        background-color: #c33122;
        padding: 0.5em 5em 0.4em 5em;
        text-shadow: 0 0 0.75em #444;
        box-shadow: 0 0 0.5em rgba(0,0,0,0.5);
        font: bold 16px/1.2em Arial, Sans-Serif;
        -webkit-text-shadow: 0 0 0.75em #444;
        -webkit-box-shadow: 0 0 0.5em rgba(0,0,0,0.5);
        -webkit-transform: rotate(-45deg) scale(0.75,1);
        z-index: 10;
        font-size: 14px;
    }
    .sold-out-strip:before {
        content: '';
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        position: absolute;
        margin: -0.3em -5em;
        transform: scale(0.7);
        -webkit-transform: scale(0.7);
        border: 2px rgba(255,255,255,0.7) dashed;
    }
`
const globalStyles = `   
    .load-more {
        position: fixed;
        bottom: 90px;
        right: 30px;
        z-index: 1001;
    }    

    .ant-card {
        margin-bottom: 20px;
    }
    .ant-card-body {
        padding: 20px 30px;
    }

    .ant-card-meta-title,
    .ant-card-meta-title h3 {
        margin: 0 !important;
    }
    .ant-card-meta-description span {
        color: #1890ff;
        cursor: pointer;
    }
    .promo-code-list {
        background: #F0F2F5;
        padding: 10px;
    }
    .ant-list-split .ant-list-item {
        border-bottom: 10px solid #F0F2F5;
        background: #fff;
        padding: 10px;
        position: relative;
        overflow: hidden;
    }
    .ant-list-item-meta-description .address {
        font-size: 13px;
        line-height: 22px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        width: 300px;
    }    
    .ant-list-item-meta-avatar {
        float: left;
    }
    .ant-list-item-meta-content {
        float: left;
    }
    .ant-avatar.ant-avatar-icon {
        height: 88px !important;
        line-height: 88px !important; 
    }    
`

class PromoCodeList extends React.Component {     

    render() {
        const { all, isLoading } = this.props;        
        const promoClassName =  (isActive, amountOfRide) => (
            classNames('promo-code', {
                'disabled': !isActive,
                'sold-out': amountOfRide === 0,
            })
        );

        const avatarClassName =  (isActive, amountOfRide) => (
            classNames('promo-code-avatar', {
                'disabled': !isActive,
                'sold-out': amountOfRide === 0,
            })
        );

        const DescriptionText = ({ venue, code, isActive, amountOfRide, radius }) => (
            <div>
                {amountOfRide===0 ? <div className="sold-out-strip"><FormattedMessage id='text.sold_out' defaultMessage='Sold out' /></div> : ''}
                <div className="address">{venue}</div>                
                <div className={promoClassName(isActive, amountOfRide)}>
                    {code}
                </div>
                <div className="promo-uses">
                    <FormattedMessage id='text.uses' defaultMessage='{count} uses remaining' values={{ count: amountOfRide }} />
                    <span> - </span>
                    <FormattedMessage id='text.radius' defaultMessage='{radius} km' values={{ radius }} />                    
                </div>
            </div>            
        );

        return (
            <div style={{width: "100%", height: "100%"}}>
                <style jsx="true">{styles}</style>
                <style global="true" jsx="true">{globalStyles}</style>
                <List
                    className="promo-code-list"
                    loading={isLoading}
                    itemLayout="horizontal"
                    dataSource={all}
                    renderItem={promoCode => (
                        <List.Item>
                            <List.Item.Meta
                                title={promoCode.eventName}
                                avatar={<Avatar size={68} icon="barcode" shape="square" className={avatarClassName(promoCode.isActive, promoCode.amountOfRide)} />}                                
                                description={
                                    <DescriptionText
                                        venue={promoCode.venueAddress}
                                        code={promoCode.id}
                                        isActive={promoCode.isActive}
                                        amountOfRide={promoCode.amountOfRide}
                                        radius={promoCode.radius}
                                    />
                                }
                            />                                             
                        </List.Item>
                    )}
                />   
            </div>
        );
    }
}

const actions = App.actions.promoCode.list;
const selectors = App.selectors.promoCode.list;
const mapStateToProps = (state) => ({
    all: selectors.all(state),
    isLoading: selectors.isLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(PromoCodeList)