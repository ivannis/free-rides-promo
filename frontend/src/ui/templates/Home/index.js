import React from 'react';
import dynamic from 'next/dynamic'
import Page from '../../components/Page'
import Layout from '../../components/Layout'

const Map = dynamic(import('../../components/Map'), {
    ssr: false
})
  
const styles = `   
    .leaflet-container {
        height: calc(100vh - 113px);
        width: 100%;
        margin: 0 auto;
    }
    .content p {
        height: calc(100vh - 113px);
        width: 100%;
        text-align: center;
        line-height: calc(100vh - 113px);
        padding: 0;
        margin: 0;
        font-size: 16px;
    }
`

export default class HomePage extends React.Component {
    render() {
        return (
            <Page title="FreeRides" description="Free rides promo application">
                <style global="true" jsx="true">{styles}</style>
                <Layout>
                    <Map />
                </Layout>
            </Page>
        );
    }
}

