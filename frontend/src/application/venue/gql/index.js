import venues from './venues';

export default {
    queries: {
        venues
    },
};
