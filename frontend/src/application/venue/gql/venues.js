import gql from 'graphql-tag';

export default gql`
    query getAllVenues {
        venues {
            id
            name
            address
            coordinate {
                latitude
                longitude
            }
        }
    }
`