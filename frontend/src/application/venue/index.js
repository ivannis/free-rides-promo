import Module from "../core/Module";
import VenueList from './VenueList'

const Venue = new Module({
    namespace: 'app', store: 'venue',
    modules: [
        VenueList
    ],
    initialState: {},
})

export default Venue;