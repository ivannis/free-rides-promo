import createRemoteObject from "../core/RemoteObject";
import { query } from '../core/effects';
import { put } from 'redux-saga/effects';
import Coordinate from "../../domain/Coordinate";
import Venue from "../../domain/Venue";
import gql from './gql';

export default createRemoteObject({
    namespace: 'venue', store: 'list'
}).extend({
    selectors: (module) => ({
        all: (state) => module.selectors.data(state)
    }),
    sagas: (module) => ({
        doFetch: function *doFetch() {
            const data = yield query(gql.queries.venues)

            return data.venues.map(
                item => new Venue(
                    item.id,
                    item.name,
                    item.address,
                    new Coordinate(item.coordinate.latitude, item.coordinate.longitude)
                )
            );
        },
        startUpSaga: function* startUpSaga() {
            // fetch promo code list
            yield put(module.actions.fetch());
        }
    })
})
