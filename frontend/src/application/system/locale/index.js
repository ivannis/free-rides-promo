import Module from "../../core/Module";
import Locale from "../../../domain/Locale";

export default new Module({
    namespace: 'app', store: 'locale',
    types: ['SWITCH'],
    reducer: (state, action, { types }) => {
        switch(action.type) {
            case types.SWITCH:
                return {
                    ...state,
                    current: action.payload
                };
            default:
                return state
        }
    },
    selectors: (module) => ({
        current: (state) => module.select(state, (localState) => localState.current),
        locales: (state) => module.select(state, (localState) => localState.locales),
        getByCode: (state, code) => module.select(state, (localState) => localState.locales.find(locale => locale.code === code))
    }),
    actions: ({ types }) => ({
        switch:  (locale) => ({ type: types.SWITCH, payload: locale })
    }),
    initialState: {
        current: new Locale(1, 'English', 'en_US'),
        locales: [
            new Locale(1, 'English', 'en_US'),
            new Locale(2, 'Español', 'es')
        ]
    }
})