import Module from './core/Module'
import PromoCode from './promoCode'
import Venue from './venue'
import Locale from './system/locale'

const App = new Module({
    namespace: 'freerides', store: 'app',    
    modules: [
        PromoCode,
        Venue,
        Locale        
    ],
    types: ['NAVIGATION_START', 'NAVIGATION_STOPED'],
    reducer: (state, action, { types }) => {
        switch(action.type) {
            case types.NAVIGATION_START:
                return {
                    ...state,
                    navigation: true
                };
            case types.NAVIGATION_STOPED:
                return {
                    ...state,
                    navigation: false
                };
            default:
                return state
        }
    },
    selectors: (module) => ({
        onNavigation: (state) => module.select(state, (localState) => localState.navigation)
    }),
    actions: ({ types }) => ({
        startNavigation:  () => ({ type: types.NAVIGATION_START }),
        stopNavigation:  () => ({ type: types.NAVIGATION_STOPED })
    }),
    initialState: {
        navigation: false        
    }
})

export default App;
