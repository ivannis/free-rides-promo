import { all, fork, put, takeEvery } from 'redux-saga/effects';
import { query, mutation } from '../core/effects';
import createRemoteObject from "../core/RemoteObject";
import PromoCodeList from './PromoCodeList'
import Coordinate from "../../domain/Coordinate";
import PromoCode from "../../domain/PromoCode";
import gql from './gql';

export default createRemoteObject({
    namespace: 'app', store: 'promoCode',    
    initialState: { data: undefined, error: undefined, loading: false }
}).extend({
    modules: [
        PromoCodeList
    ],
    types: ['VALIDATE', 'USE'],
    selectors: (module) => ({
        current: (state) => module.selectors.data(state)
    }),
    actions: ({ types }) => ({
        validate:  (promoCodeId, origin, destination) => ({ type: types.VALIDATE, payload: { promoCodeId, origin, destination } }),
        use:  (promoCodeId, origin, destination) => ({ type: types.USE, payload: { promoCodeId, origin, destination } })
    }),   
    sagas: (module, parentModule) => ({
        rootSaga: function * rootSaga() {
            yield all([
                fork(parentModule.rootSaga),
                takeEvery(module.types.VALIDATE, module.sagas.doValidation),
                takeEvery(module.types.USE, module.sagas.doRide)
            ]);
        },
        doValidation: function *doValidation(action) {
            yield put(module.actions.fetch(action.payload));
        },
        doFetch: function *doFetch(payload) {
            const data = yield query(gql.queries.validatePromoCode, payload)              
            const promoCode = data.promoCode;

            return new PromoCode(
                promoCode.id,
                promoCode.radius,
                promoCode.amountOfRide,
                promoCode.eventId,
                promoCode.eventName,
                promoCode.venueId,
                promoCode.venueAddress,
                new Coordinate(promoCode.coordinate.latitude, promoCode.coordinate.longitude),
                promoCode.isActive,
                promoCode.isExpired,
                promoCode.polyline
            )
        },
        doRide: function *doRide(action) {
            yield put(module.actions.post(action.payload));
        },
        doPost: function *doPost(payload) {
            const data = yield mutation(gql.mutations.usePromoCode, payload) 
            const promoCode = data.promoCode;

            // update the list                         
            yield put(PromoCodeList.actions.fetch());

            return new PromoCode(
                promoCode.id,
                promoCode.radius,
                promoCode.amountOfRide,
                promoCode.eventId,
                promoCode.eventName,
                promoCode.venueId,
                promoCode.venueAddress,
                new Coordinate(promoCode.coordinate.latitude, promoCode.coordinate.longitude),
                promoCode.isActive,
                promoCode.isExpired
            )
        }
    })
})