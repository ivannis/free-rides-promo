import createRemoteObject from "../core/RemoteObject";
import { query } from '../core/effects';
import { put } from 'redux-saga/effects';
import Coordinate from "../../domain/Coordinate";
import PromoCode from "../../domain/PromoCode";
import gql from './gql';

export default createRemoteObject({
    namespace: 'promoCode', store: 'list'
}).extend({
    selectors: (module) => ({
        all: (state) => module.selectors.data(state),
        actives: (state, code) => module.select(state, (localState) => localState.data.find(promoCode => promoCode.isActive))
    }),
    sagas: (module) => ({
        doFetch: function *doFetch() {
            const data = yield query(gql.queries.allPromoCodes)

            return data.promoCodes.map(
                item => new PromoCode(
                    item.id,
                    item.radius,
                    item.amountOfRide,
                    item.eventId,
                    item.eventName,
                    item.venueId,
                    item.venueAddress,
                    new Coordinate(item.coordinate.latitude, item.coordinate.longitude),
                    item.isActive,
                    item.isExpired
                )
            );
        },
        startUpSaga: function* startUpSaga() {
            // fetch promo code list
            yield put(module.actions.fetch());
        }
    })
})
