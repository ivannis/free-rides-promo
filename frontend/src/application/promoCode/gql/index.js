import allPromoCodes from './query/allPromoCodes';
import validatePromoCode from './query/validatePromoCode';
import usePromoCode from './mutation/usePromoCode';

export default {
    queries: {
        allPromoCodes,
        validatePromoCode
    },
    mutations: {
        usePromoCode
    },
};
