import gql from 'graphql-tag';

export default gql`
    query getAllPromoCodes {
        promoCodes {
            id
            radius
            amountOfRide
            eventId
            eventName
            venueId
            venueAddress
            coordinate {
                latitude
                longitude
            }
            isActive
            isExpired
        }
    }
`