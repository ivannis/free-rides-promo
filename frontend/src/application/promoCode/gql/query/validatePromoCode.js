import gql from 'graphql-tag';

export default gql`
    query validate($promoCodeId: String!, $origin: String!, $destination: String!) {
        promoCode: validatePromoCode(promoCodeId: $promoCodeId, origin: $origin, destination: $destination) {
            id
            radius
            amountOfRide
            eventId
            eventName
            venueId
            venueAddress
            coordinate {
                latitude
                longitude
            }
            isActive
            isExpired
            polyline
        }
    }
`