import gql from 'graphql-tag';

export default gql`
    mutation use($promoCodeId: String!, $origin: String!, $destination: String!) {
        promoCode: usePromoCode(promoCodeId: $promoCodeId, origin: $origin, destination: $destination) {
            id
            radius
            amountOfRide
            eventId
            eventName
            venueId
            venueAddress
            coordinate {
                latitude
                longitude
            }
            isActive
            isExpired
        }
    }
`