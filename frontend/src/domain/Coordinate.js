// @flow
export interface ICoordinate {
    latitude: number;
    longitude: number;
}

export default class Coordinate implements ICoordinate {
    latitude: number;
    longitude: number;

    constructor(latitude: number, longitude: number) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}