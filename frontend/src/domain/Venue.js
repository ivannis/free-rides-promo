// @flow

import { ICoordinate } from "./Coordinate";

export interface IVenue {
    id: string;
    name: string;
    address: string;
    coordinate: ICoordinate;
}

export default class Venue implements IVenue {
    id: string;
    name: string;
    address: string;
    coordinate: ICoordinate;

    constructor(
        id: string,
        name: string,
        address: string,
        coordinate: ICoordinate
    ) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.coordinate = coordinate;
    }
}