// @flow

import { ICoordinate } from "./Coordinate";

export interface IPromoCode {
    id: string;
    radius: number;
    amountOfRide: string;
    eventId: string;
    eventName: string;
    venueId: string;
    venueAddress: string;
    coordinate: ICoordinate;
    isActive: boolean;
    isExpired: boolean;
    polyline: string;
}

export default class PromoCode implements IPromoCode {
    id: string;
    radius: number;
    amountOfRide: string;
    eventId: string;    
    eventName: string;
    venueId: string;
    venueAddress: string;
    coordinate: ICoordinate;
    isActive: boolean;
    isExpired: boolean;
    polyline: string;

    constructor(
        id: string,
        radius: number,
        amountOfRide: string,
        eventId: string,
        eventName: string,
        venueId: string,
        venueAddress: string,
        coordinate: ICoordinate,
        isActive: boolean,
        isExpired: boolean,
        polyline?: string
    ) {
        this.id = id;
        this.radius = radius;
        this.amountOfRide = amountOfRide;
        this.eventId = eventId;
        this.eventName = eventName;
        this.venueId = venueId;
        this.venueAddress = venueAddress;
        this.coordinate = coordinate;
        this.isActive = isActive;
        this.isExpired = isExpired;
        this.polyline = polyline;
    }
}