import React from 'react'

export const IntlContext = React.createContext();

export default Component => {
    return class WithIntlContext extends React.Component {        
        static async getInitialProps (context) {
            if (typeof Component.getInitialProps === 'function') {
                return await Component.getInitialProps(context)
            }

            return {}
        }

        render() {
            const { intl } = this.props

            return (
                <IntlContext.Provider value={intl}>
                    <Component {...this.props} />
                </IntlContext.Provider>                
            )
        }
    }
}