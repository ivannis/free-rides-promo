<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Core\Infrastructure\GraphQL\Type;

use Cubiche\Domain\Geolocation\Coordinate;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\FloatType;

/**
 * CoordinateType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CoordinateType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('latitude', [
                'type' => new NonNullType(new FloatType()),
                'resolve' => function (Coordinate $coorditane) {
                    return $coorditane->latitude()->toNative();
                },
            ])
            ->addField('longitude', [
                'type' => new NonNullType(new FloatType()),
                'resolve' => function (Coordinate $coorditane) {
                    return $coorditane->longitude()->toNative();
                },
            ])
        ;
    }
}
