<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Core\Infrastructure\GraphQL;

use Cubiche\Core\Validator\Exception\ValidationException;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Mutation\ChangePromoCodeRadius;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Mutation\DeactivatePromoCode;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Mutation\UsePromoCode;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Query\FindActivesPromoCodes;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Query\FindAllPromoCodes;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Query\FindOnePromoCode;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\Query\ValidatePromoCode;
use FreeRides\Events\Infrastructure\Event\GraphQL\Mutation\CreateEvent;
use FreeRides\Events\Infrastructure\Event\GraphQL\Mutation\GeneratePromoCode;
use FreeRides\Events\Infrastructure\Event\GraphQL\Query\FindAllEvents;
use FreeRides\Events\Infrastructure\Event\GraphQL\Query\FindOneEvent;
use FreeRides\Location\Infrastructure\Venue\GraphQL\Query\FindAllVenues;
use FreeRides\Location\Infrastructure\Venue\GraphQL\Query\FindOneVenue;
use FreeRides\System\Infrastructure\Language\GraphQL\Query\FindAllLanguages;
use FreeRides\System\Infrastructure\Language\GraphQL\Query\FindOneLanguage;
use Youshido\GraphQL\Config\Schema\SchemaConfig;
use Youshido\GraphQL\Exception\Interfaces\LocationableExceptionInterface;
use Youshido\GraphQL\Schema\AbstractSchema;

/**
 * ApplicationSchema class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class ApplicationSchema extends AbstractSchema
{
    /**
     * {@inheritdoc}
     */
    public function build(SchemaConfig $config)
    {
        $config->getQuery()->addFields([
            new FindAllLanguages(),
            new FindOneLanguage(),
            new FindAllVenues(),
            new FindOneVenue(),
            new FindAllEvents(),
            new FindOneEvent(),
            new FindAllPromoCodes(),
            new FindActivesPromoCodes(),
            new FindOnePromoCode(),
            new ValidatePromoCode()
        ]);

        $config->getMutation()->addFields([
            new CreateEvent(),
            new GeneratePromoCode(),
            new ChangePromoCodeRadius(),
            new DeactivatePromoCode(),
            new UsePromoCode()
        ]);
    }

    /**
     * @param array $errors
     *
     * @return array
     */
    public static function formatErrors(array $errors)
    {
        $result = [];
        foreach ($errors as $error) {
            if ($error instanceof ValidationException) {
                $validationErrors = array(
                    'message' => $error->getMessage(),
                    'code' => $error->getCode(),
                    'errors' => array(),
                );

                foreach ($error->getErrorExceptions() as $errorException) {
                    $validationErrors['errors'][] = array(
                        'message' => $errorException->getMessage(),
                        'code' => $errorException->getCode(),
                        'path' => $errorException->getPropertyPath(),
                    );
                }

                $result[] = $validationErrors;
            } elseif ($error instanceof LocationableExceptionInterface) {
                $result[] = array_merge(
                    ['message' => $error->getMessage()],
                    $error->getLocation() ? ['locations' => [$error->getLocation()->toArray()]] : [],
                    $error->getCode() ? ['code' => $error->getCode()] : []
                );
            } else {
                $result[] = array_merge(
                    ['message' => $error->getMessage()],
                    $error->getCode() ? ['code' => $error->getCode()] : []
                );
            }
        }

        return $result;
    }
}
