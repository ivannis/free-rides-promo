<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Domain\Language\Event;

use FreeRides\System\Domain\Language\LanguageId;
use Cubiche\Domain\EventSourcing\DomainEvent;

/**
 * LanguageWasEnabled class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LanguageWasEnabled extends DomainEvent
{
    /**
     * LanguageWasEnabled constructor.
     *
     * @param LanguageId $languageId
     */
    public function __construct(LanguageId $languageId)
    {
        parent::__construct($languageId);
    }

    /**
     * @return LanguageId
     */
    public function languageId()
    {
        return $this->aggregateId();
    }
}
