<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Infrastructure\Language\GraphQL\Mutation;

use FreeRides\Core\Infrastructure\GraphQL\Type\LocalizableType;
use FreeRides\System\Application\Language\Controller\LanguageWriteController;
use FreeRides\System\Application\Language\Controller\LanguageReadController;
use FreeRides\System\Application\Language\ReadModel\Language;
use FreeRides\System\Infrastructure\Language\GraphQL\LanguageType;
use Cubiche\Domain\Localizable\LocalizableValueInterface;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * CreateLanguage class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateLanguage extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('code', new NonNullType(new StringType()))
            ->addArgument('name', new NonNullType(new LocalizableType()))
        ;
    }

    /**¯
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Language
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var LanguageWriteController $controller */
        $controller = $info->getContainer()->get('app.write_controller.language');

        $controller->createAction(
            $args['code'],
            $args['name'],
            LocalizableValueInterface::DEFAULT_LOCALE
        );

        /** @var LanguageReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.language');

        return $controller->findOneByCodeAction($args['code']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'createLanguage';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new LanguageType();
    }
}
