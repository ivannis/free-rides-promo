<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Infrastructure\Language\GraphQL;

use FreeRides\System\Application\Language\ReadModel\Language;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * LanguageType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LanguageType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (Language $language, $args) {
                    return $language->id()->toNative();
                },
            ])
            ->addField('name', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Language $language, $args) {
                    return $language->name()->toNative();
                },
            ])
            ->addField('code', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Language $language, $args) {
                    return $language->code()->toNative();
                },
            ])
        ;
    }
}
