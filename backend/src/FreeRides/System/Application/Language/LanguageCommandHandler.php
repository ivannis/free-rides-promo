<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Application\Language;

use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\System\Application\Language\Command\CreateLanguageCommand;
use FreeRides\System\Application\Language\Command\DisableLanguageCommand;
use FreeRides\System\Application\Language\Command\EnableLanguageCommand;
use FreeRides\System\Application\Language\Command\UpdateLanguageNameCommand;
use Cubiche\Domain\Locale\LanguageCode;
use Cubiche\Domain\Localizable\LocalizableString;
use Cubiche\Domain\Repository\RepositoryInterface;
use FreeRides\System\Domain\Language\Language;
use FreeRides\System\Domain\Language\LanguageId;

/**
 * LanguageCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LanguageCommandHandler
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * LanguageCommandHandler constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateLanguageCommand $command
     */
    public function createLanguage(CreateLanguageCommand $command)
    {
        $language = new Language(
            LanguageId::fromNative($command->languageId()),
            LocalizableString::fromArray($command->name(), $command->defaultLocale()),
            LanguageCode::fromNative($command->code())
        );

        $this->repository->persist($language);
    }

    /**
     * @param UpdateLanguageNameCommand $command
     */
    public function updateLanguageName(UpdateLanguageNameCommand $command)
    {
        $language = $this->findOr404($command->languageId());
        $language->updateName(LocalizableString::fromArray($command->name()));

        $this->repository->persist($language);
    }

    /**
     * @param EnableLanguageCommand $command
     */
    public function enableLanguage(EnableLanguageCommand $command)
    {
        $language = $this->findOr404($command->languageId());
        $language->enable();

        $this->repository->persist($language);
    }

    /**
     * @param DisableLanguageCommand $command
     */
    public function disableLanguage(DisableLanguageCommand $command)
    {
        $language = $this->findOr404($command->languageId());
        $language->disable();

        $this->repository->persist($language);
    }

    /**
     * @param string $languageId
     *
     * @return Language
     */
    private function findOr404($languageId)
    {
        /** @var Language $language */
        $language = $this->repository->get(LanguageId::fromNative($languageId));
        if ($language === null) {
            throw new NotFoundException(sprintf(
                'There is no language with id: %s',
                $languageId
            ));
        }

        return $language;
    }
}
