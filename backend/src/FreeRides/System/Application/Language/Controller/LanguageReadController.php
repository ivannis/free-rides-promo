<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Application\Language\Controller;

use Cubiche\Core\Collections\CollectionInterface;
use FreeRides\Core\Application\Controller\QueryController;
use FreeRides\System\Application\Language\ReadModel\Language;
use FreeRides\System\Application\Language\ReadModel\Query\FindAllLanguages;
use FreeRides\System\Application\Language\ReadModel\Query\FindOneLanguageByCode;

/**
 * LanguageReadController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LanguageReadController extends QueryController
{
    /**
     * @return CollectionInterface|Language[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(new FindAllLanguages());
    }

    /**
     * @param string $code
     *
     * @return Language|null
     */
    public function findOneByCodeAction($code)
    {
        return $this->queryBus()->dispatch(
            new FindOneLanguageByCode($code)
        );
    }
}
