<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\System\Application\Language\Controller;

use FreeRides\Core\Application\Controller\CommandController;
use FreeRides\System\Application\Language\Command\CreateLanguageCommand;
use FreeRides\System\Application\Language\Command\DisableLanguageCommand;
use FreeRides\System\Application\Language\Command\EnableLanguageCommand;
use FreeRides\System\Application\Language\Command\UpdateLanguageNameCommand;
use FreeRides\System\Domain\Language\LanguageId;

/**
 * LanguageWriteController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class LanguageWriteController extends CommandController
{
    /**
     * @param string $code
     * @param array  $name
     * @param string $defaultLocale
     *
     * @return string
     */
    public function createAction($code, array $name, $defaultLocale)
    {
        $languageId = LanguageId::next()->toNative();
        $this->commandBus()->dispatch(
            new CreateLanguageCommand($languageId, $code, $name, $defaultLocale)
        );

        return $languageId;
    }

    /**
     * @param string $languageId
     * @param array  $name
     *
     * @return string
     */
    public function updateNameAction($languageId, array $name)
    {
        $this->commandBus()->dispatch(
            new UpdateLanguageNameCommand($languageId, $name)
        );

        return true;
    }

    /**
     * @param string $languageId
     *
     * @return bool
     */
    public function disableAction($languageId)
    {
        $this->commandBus()->dispatch(
            new DisableLanguageCommand($languageId)
        );

        return true;
    }

    /**
     * @param string $languageId
     *
     * @return bool
     */
    public function enableAction($languageId)
    {
        $this->commandBus()->dispatch(
            new EnableLanguageCommand($languageId)
        );

        return true;
    }
}
