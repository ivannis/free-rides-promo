<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * CreateVenueCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateVenueCommand extends Command
{
    /**
     * @var string
     */
    protected $venueId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $address;

    /**
     * CreateVenueCommand constructor.
     *
     * @param string $venueId
     * @param string $name
     * @param string $address
     */
    public function __construct($venueId, $name, $address)
    {
        $this->venueId = $venueId;
        $this->name = $name;
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function address()
    {
        return $this->address;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('venueId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('name', Assertion::string()->notBlank());
        $classMetadata->addPropertyConstraint('address', Assertion::string()->notBlank());
    }
}
