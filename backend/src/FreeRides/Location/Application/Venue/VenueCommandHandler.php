<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue;

use Cubiche\Domain\Repository\RepositoryInterface;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Location\Application\Venue\Command\CreateVenueCommand;
use FreeRides\Location\Application\Address\Service\AddressGeocoderInterface;
use FreeRides\Location\Domain\Address\Address;
use FreeRides\Location\Domain\Venue\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueCommandHandler
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var AddressGeocoderInterface
     */
    protected $geocoder;

    /**
     * VenueCommandHandler constructor.
     *
     * @param RepositoryInterface      $repository
     * @param AddressGeocoderInterface $geocoder
     */
    public function __construct(RepositoryInterface $repository, AddressGeocoderInterface $geocoder)
    {
        $this->repository = $repository;
        $this->geocoder = $geocoder;
    }

    /**
     * @param CreateVenueCommand $command
     */
    public function createVenue(CreateVenueCommand $command)
    {
        $address = $this->findAddressOr404($command->address());

        $venue = new Venue(
            VenueId::fromNative($command->venueId()),
            StringLiteral::fromNative($command->name()),
            $address->addressId(),
            $address->formattedAddress(),
            $address->coordinate()
        );

        $this->repository->persist($venue);
    }

    /**
     * @param string $street
     *
     * @return Address
     */
    private function findAddressOr404($street)
    {
        $address = $this->geocoder->geocode(StringLiteral::fromNative($street));
        if ($address === null) {
            throw new NotFoundException(sprintf(
                'There is no geocode information for the given address "%s"',
                $street
            ));
        }

        return $address;
    }
}
