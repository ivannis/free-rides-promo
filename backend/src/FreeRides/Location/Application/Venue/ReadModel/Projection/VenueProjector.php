<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\ReadModel\Projection;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Location\Domain\Venue\Event\VenueWasCreated;
use FreeRides\Location\Application\Venue\ReadModel\Venue;

/**
 * VenueProjector class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueProjector implements DomainEventSubscriberInterface
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * Projector constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param VenueWasCreated $event
     */
    public function whenVenueWasCreated(VenueWasCreated $event)
    {
        $venue = new Venue(
            $event->venueId(),
            $event->name(),
            $event->address()->formattedAddress(),
            $event->address()->coordinate()
        );

        $this->repository->persist($venue);
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            VenueWasCreated::class => array('whenVenueWasCreated'),
        );
    }
}
