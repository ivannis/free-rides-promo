<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\ReadModel;

use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindAllVenues;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindOneVenueById;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * CurrencyQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllVenues $query
     *
     * @return Venue
     */
    public function findAllVenues(FindAllVenues $query)
    {
        return $this->repository->getIterator();
    }

    /**
     * @param FindOneVenueById $query
     *
     * @return Venue
     */
    public function findOneVenueById(FindOneVenueById $query)
    {
        return $this->repository->findOne(
            Criteria::property('id')->eq(VenueId::fromNative($query->venueId()))
        );
    }
}
