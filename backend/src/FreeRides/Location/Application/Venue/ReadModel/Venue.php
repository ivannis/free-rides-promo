<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\ReadModel;

use Cubiche\Domain\EventSourcing\ReadModelInterface;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * Venue class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Venue extends Entity implements ReadModelInterface
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var StringLiteral
     */
    protected $formattedAddress;

    /**
     * @var Coordinate
     */
    protected $coordinate;

    /**
     * Venue constructor.
     *
     * @param VenueId       $venueId
     * @param StringLiteral $name
     * @param StringLiteral $formattedAddress
     * @param Coordinate    $coordinate
     */
    public function __construct(
        VenueId $venueId,
        StringLiteral $name,
        StringLiteral $formattedAddress,
        Coordinate $coordinate
    ) {
        parent::__construct($venueId);

        $this->name = $name;
        $this->formattedAddress = $formattedAddress;
        $this->coordinate = $coordinate;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->id;
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return StringLiteral
     */
    public function formattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * @return Coordinate
     */
    public function coordinate()
    {
        return $this->coordinate;
    }
}
