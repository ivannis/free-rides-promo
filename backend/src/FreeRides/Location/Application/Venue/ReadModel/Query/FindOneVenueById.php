<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * FindOneVenueById class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindOneVenueById extends Query
{
    /**
     * @var string
     */
    protected $venueId;

    /**
     * FindOneVenueById constructor.
     *
     * @param string $venueId
     */
    public function __construct($venueId)
    {
        $this->venueId = $venueId;
    }

    /**
     * @return string
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('venueId', Assertion::uuid()->notBlank());
    }
}
