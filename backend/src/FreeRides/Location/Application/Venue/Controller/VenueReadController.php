<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\Controller;

use FreeRides\Core\Application\Controller\QueryController;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindAllVenues;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindOneVenueById;
use FreeRides\Location\Application\Venue\ReadModel\Venue;

/**
 * VenueReadController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueReadController extends QueryController
{
    /**
     * @return Venue[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(
            new FindAllVenues()
        );
    }

    /**
     * @param string $venueId
     *
     * @return Venue
     */
    public function findOneByIdAction($venueId)
    {
        return $this->queryBus()->dispatch(
            new FindOneVenueById($venueId)
        );
    }
}
