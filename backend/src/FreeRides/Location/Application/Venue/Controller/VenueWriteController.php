<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Venue\Controller;

use FreeRides\Core\Application\Controller\CommandController;
use FreeRides\Location\Application\Venue\Command\CreateVenueCommand;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueWriteController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueWriteController extends CommandController
{
    /**
     * @param string $name
     * @param string $address
     *
     * @return string
     */
    public function createAction($name, $address)
    {
        $venueId = VenueId::next()->toNative();
        $this->commandBus()->dispatch(
            new CreateVenueCommand($venueId, $name, $address)
        );

        return $venueId;
    }
}
