<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Address\Controller;

use Cubiche\Domain\System\StringLiteral;
use FreeRides\Core\Application\Controller\QueryController;
use FreeRides\Location\Application\Address\ReadModel\Query\FindDirectionsRoute;
use FreeRides\Location\Application\Address\ReadModel\Query\FindOneAddressByName;
use FreeRides\Location\Domain\Address\Address;

/**
 * AddressReadController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class AddressReadController extends QueryController
{
    /**
     * @param string $address
     *
     * @return Address|null
     */
    public function geocodeAction($address)
    {
        return $this->queryBus()->dispatch(
            new FindOneAddressByName($address)
        );
    }

    /**
     * @param float $originLatitude
     * @param float $originLongitude
     * @param float $destinationLatitude
     * @param float $destinationLongitude
     *
     * @return StringLiteral
     */
    public function directionsAction($originLatitude, $originLongitude, $destinationLatitude, $destinationLongitude)
    {
        return $this->queryBus()->dispatch(
            new FindDirectionsRoute($originLatitude, $originLongitude, $destinationLatitude, $destinationLongitude)
        );
    }
}
