<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Address\Service;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;

/**
 * DirectionsGeocoder interface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface DirectionsGeocoderInterface
{
    /**
     * @param Coordinate $origin
     * @param Coordinate $destination
     *
     * @return StringLiteral|null
     */
    public function directions(Coordinate $origin, Coordinate $destination);
}
