<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Address\Service;

use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Domain\Address\Address;

/**
 * AddressGeocoder interface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface AddressGeocoderInterface
{
    /**
     * @param StringLiteral $address
     *
     * @return Address|null
     */
    public function geocode(StringLiteral $address);
}
