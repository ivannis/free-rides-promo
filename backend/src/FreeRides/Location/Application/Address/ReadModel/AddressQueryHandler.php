<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Address\ReadModel;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Address\ReadModel\Query\FindDirectionsRoute;
use FreeRides\Location\Application\Address\ReadModel\Query\FindOneAddressByName;
use FreeRides\Location\Application\Address\Service\AddressGeocoderInterface;
use FreeRides\Location\Application\Address\Service\DirectionsGeocoderInterface;
use FreeRides\Location\Domain\Address\Address;

/**
 * AddressQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class AddressQueryHandler
{
    /**
     * @var AddressGeocoderInterface
     */
    protected $geocoder;

    /**
     * @var DirectionsGeocoderInterface
     */
    protected $router;

    /**
     * AddressQueryHandler constructor.
     *
     * @param AddressGeocoderInterface    $geocoder
     * @param DirectionsGeocoderInterface $router
     */
    public function __construct(AddressGeocoderInterface $geocoder, DirectionsGeocoderInterface $router)
    {
        $this->geocoder = $geocoder;
        $this->router = $router;
    }

    /**
     * @param FindOneAddressByName $query
     *
     * @return Address|null
     */
    public function findOneAddressByName(FindOneAddressByName $query)
    {
        return $this->geocoder->geocode(StringLiteral::fromNative($query->address()));
    }

    /**
     * @param FindDirectionsRoute $query
     *
     * @return StringLiteral
     */
    public function findDirectionsRoute(FindDirectionsRoute $query)
    {
        return $this->router->directions(
            Coordinate::fromLatLng($query->originLatitude(), $query->originLongitude()),
            Coordinate::fromLatLng($query->destinationLatitude(), $query->destinationLongitude())
        );
    }
}
