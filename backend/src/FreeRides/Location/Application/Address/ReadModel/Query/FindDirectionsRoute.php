<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Address\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * FindDirectionsRoute class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindDirectionsRoute extends Query
{
    /**
     * @var float
     */
    protected $originLatitude;

    /**
     * @var float
     */
    protected $originLongitude;

    /**
     * @var float
     */
    protected $destinationLatitude;

    /**
     * @var float
     */
    protected $destinationLongitude;

    /**
     * FindDirectionsRoute constructor.
     *
     * @param float $originLatitude
     * @param float $originLongitude
     * @param float $destinationLatitude
     * @param float $destinationLongitude
     */
    public function __construct($originLatitude, $originLongitude, $destinationLatitude, $destinationLongitude)
    {
        $this->originLatitude = $originLatitude;
        $this->originLongitude = $originLongitude;
        $this->destinationLatitude = $destinationLatitude;
        $this->destinationLongitude = $destinationLongitude;
    }

    /**
     * @return float
     */
    public function originLatitude()
    {
        return $this->originLatitude;
    }

    /**
     * @return float
     */
    public function originLongitude()
    {
        return $this->originLongitude;
    }

    /**
     * @return float
     */
    public function destinationLatitude()
    {
        return $this->destinationLatitude;
    }

    /**
     * @return float
     */
    public function destinationLongitude()
    {
        return $this->destinationLongitude;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('originLatitude', Assertion::float()->notBlank());
        $classMetadata->addPropertyConstraint('originLongitude', Assertion::float()->notBlank());
        $classMetadata->addPropertyConstraint('destinationLatitude', Assertion::float()->notBlank());
        $classMetadata->addPropertyConstraint('destinationLongitude', Assertion::float()->notBlank());
    }
}
