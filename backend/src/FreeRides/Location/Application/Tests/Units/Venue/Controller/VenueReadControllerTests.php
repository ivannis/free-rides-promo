<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\Controller;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Application\Venue\Controller\VenueReadController;
use FreeRides\Location\Application\Venue\ReadModel\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueReadControllerTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueReadControllerTests extends TestCase
{
    /**
     * @return VenueReadController
     */
    protected function createController()
    {
        return new VenueReadController($this->queryBus());
    }

    /**
     * Test FindAllAction method.
     */
    public function testFindAllAction()
    {
        $this
            ->given($controller = $this->createController())
            ->and($repository = $this->queryRepository(Venue::class))
            ->then()
                ->array(iterator_to_array($controller->findAllAction()))
                    ->isEmpty()
            ->and()
            ->when(
                $repository->persist(
                    new Venue(
                        VenueId::next(),
                        StringLiteral::fromNative('Sagrada Familia'),
                        StringLiteral::fromNative('Carrer de Mallorca, 401, Barcelona'),
                        Coordinate::fromLatLng(41.404005, 2.174893)
                    )
                )
            )
            ->then()
            ->array(iterator_to_array($controller->findAllAction()))
                ->isNotEmpty()
                    ->hasSize(1)
        ;
    }

    /**
     * Test FindOneByIdAction method.
     */
    public function testFindOneByIdAction()
    {
        $this
            ->given($controller = $this->createController())
            ->and($repository = $this->queryRepository(Venue::class))
            ->and($venueId = VenueId::nextUUIDValue())
            ->then()
                ->variable($controller->findOneByIdAction($venueId))
                    ->isNull()
            ->and()
            ->when(
                $repository->persist(
                    new Venue(
                        VenueId::fromNative($venueId),
                        StringLiteral::fromNative('Sagrada Familia'),
                        StringLiteral::fromNative('Carrer de Mallorca, 401, Barcelona'),
                        Coordinate::fromLatLng(41.404005, 2.174893)
                    )
                )
            )
            ->then()
                ->object($controller->findOneByIdAction($venueId))
                    ->isInstanceOf(Venue::class)
        ;
    }
}
