<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\ReadModel;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindAllVenues;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindOneVenueById;
use FreeRides\Location\Application\Venue\ReadModel\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueQueryHandlerTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueQueryHandlerTests extends TestCase
{
    /**
     * @param VenueId|null $venueId
     */
    protected function addVenueToRepository(VenueId $venueId = null)
    {
        $repository = $this->queryRepository(Venue::class);

        $repository->persist(
            new Venue(
                $venueId ?  $venueId : VenueId::next(),
                StringLiteral::fromNative('Sagrada Familia'),
                StringLiteral::fromNative('Carrer de Mallorca, 401, Barcelona'),
                Coordinate::fromLatLng(41.404005, 2.174893)
            )
        );
    }

    /**
     * Test FindAllVenues method.
     */
    public function testFindAllVenues()
    {
        $this
            ->given($query = new FindAllVenues())
            ->then()
                ->array(iterator_to_array($this->queryBus()->dispatch($query)))
                    ->isEmpty()
            ->and()
            ->when($this->addVenueToRepository())
            ->then()
                ->array(iterator_to_array($this->queryBus()->dispatch($query)))
                    ->isNotEmpty()
                    ->hasSize(1)
        ;
    }

    /**
     * Test FindOneVenueById method.
     */
    public function testFindOneVenueById()
    {
        $this
            ->given($venueId = VenueId::next())
            ->and($query = new FindOneVenueById($venueId->toNative()))
            ->then()
                ->variable($this->queryBus()->dispatch($query))
                    ->isNull()
            ->and()
            ->when($this->addVenueToRepository($venueId))
            ->then()
                ->object($this->queryBus()->dispatch($query))
                    ->isInstanceOf(Venue::class)
        ;
    }
}
