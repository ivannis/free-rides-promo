<?php


/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\Controller;

use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Application\Venue\Controller\VenueWriteController;
use FreeRides\Location\Application\Venue\ReadModel\Query\FindAllVenues;
use FreeRides\Location\Application\Venue\ReadModel\Venue;

/**
 * VenueWriteControllerTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueWriteControllerTests extends TestCase
{
    /**
     * @return VenueWriteController
     */
    protected function createController()
    {
        return new VenueWriteController($this->commandBus());
    }

    /**
     * @return Venue
     */
    protected function findAllVenues()
    {
        return $this->queryBus()->dispatch(new FindAllVenues());
    }

    /**
     * Test CreateAction method.
     */
    public function testCreateAction()
    {
        $this
            ->given($controller = $this->createController())
            ->and($name = 'Sagrada Familia')
            ->and($address = 'Carrer de Mallorca, 401, Barcelona')
            ->when($venue = $this->findAllVenues())
            ->then()
                ->integer($venue->count())
                    ->isEqualTo(0)
            ->and()
            ->when($controller->createAction($name, $address))
            ->and($venue = $this->findAllVenues())
            ->then()
                ->integer($venue->count())
                    ->isEqualTo(1)
                ->and()
                ->exception(function () use ($controller, $name) {
                    $controller->createAction($name, 'Fake address');
                })->isInstanceOf(NotFoundException::class)
        ;
    }
}
