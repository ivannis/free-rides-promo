<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue;

use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Application\Venue\Command\CreateVenueCommand;
use FreeRides\Location\Domain\Venue\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueCommandHandlerTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueCommandHandlerTests extends TestCase
{
    /**
     * Test CreateVenue method.
     */
    public function testCreateVenue()
    {
        $this
            ->given($venueId = VenueId::next())
            ->and(
                $command = new CreateVenueCommand(
                    $venueId->toNative(),
                    'Sagrada Familia',
                    'Carrer de Mallorca, 401, Barcelona'
                )
            )
            ->when($repository = $this->writeRepository(Venue::class))
            ->then()
                ->variable($repository->get($venueId))
                    ->isNull()
            ->and()
            ->when($this->commandBus()->dispatch($command))
            ->then()
                ->object($venue = $repository->get($venueId))
                    ->isInstanceOf(Venue::class)
                ->string($venue->name()->toNative())
                    ->isEqualTo('Sagrada Familia')
                ->string($venue->address()->formattedAddress()->toNative())
                    ->isEqualTo('Carrer de Mallorca, 401, Barcelona')
                ->and()
                ->exception(function () {
                    $this->commandBus()->dispatch(
                        new CreateVenueCommand(
                            VenueId::nextUUIDValue(),
                            'Sagrada Familia',
                            'Invalid address'
                        )
                    );
                })->isInstanceOf(NotFoundException::class)
        ;
    }
}
