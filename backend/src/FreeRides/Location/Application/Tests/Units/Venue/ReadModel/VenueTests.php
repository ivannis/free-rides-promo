<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\ReadModel;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Core\Domain\Tests\Units\ReadModel\ReadModelTestTrait;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueTests extends TestCase
{
    use ReadModelTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getArguments()
    {
        return array(
            VenueId::next(),
            StringLiteral::fromNative('Sagrada Familia'),
            StringLiteral::fromNative('Carrer de Mallorca, 401, Barcelona'),
            Coordinate::fromLatLng(41.404005, 2.174893)
        );
    }
}
