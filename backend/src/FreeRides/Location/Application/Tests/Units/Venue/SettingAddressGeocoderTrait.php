<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Address\Service\AddressGeocoderInterface;
use FreeRides\Location\Domain\Address\Address;
use FreeRides\Location\Domain\Address\AddressId;

/**
 * SettingAddressGeocoder trait..
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
trait SettingAddressGeocoderTrait
{
    /**
     * @return AddressGeocoderInterface
     */
    protected function addressGeocoder()
    {
        $geocoderMock = $this->newMockInstance(AddressGeocoderInterface::class);
        $this->calling($geocoderMock)->geocode = function (StringLiteral $address) {
            if ($address->toNative() == 'Carrer de Mallorca, 401, Barcelona') {
                return new Address(
                    AddressId::fromNative('ChIJ0YdHm-eipBIRZgXp5nBJQtg'),
                    $address,
                    Coordinate::fromLatLng(41.404005, 2.174893)
                );
            }

            return null;
        };

        return $geocoderMock;
    }
}
