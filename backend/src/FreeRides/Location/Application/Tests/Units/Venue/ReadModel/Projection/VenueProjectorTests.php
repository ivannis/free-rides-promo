<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\ReadModel\Projection;

use Cubiche\Domain\EventSourcing\ReadModelInterface;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Application\Venue\Command\CreateVenueCommand;
use FreeRides\Location\Application\Venue\ReadModel\Projection\VenueProjector;
use FreeRides\Location\Application\Venue\ReadModel\Venue as ReadModelVenue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueProjectorTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueProjectorTests extends TestCase
{
    /**
     * Test create method.
     */
    public function testCreate()
    {
        $this
            ->given(
                $projector = new VenueProjector(
                    $this->queryRepository(ReadModelVenue::class)
                )
            )
            ->then()
                ->array($projector->getSubscribedEvents())
                    ->isNotEmpty()
        ;
    }

    /**
     * Test WhenVenueWasCreated method.
     */
    public function testWhenVenueWasCreated()
    {
        $this
            ->given($repository = $this->queryRepository(ReadModelVenue::class))
            ->and(
                $venueId = VenueId::next(),
                $command = new CreateVenueCommand(
                    $venueId->toNative(),
                    'Sagrada Familia',
                    'Carrer de Mallorca, 401, Barcelona'
                )
            )
            ->then()
                ->boolean($repository->isEmpty())
                    ->isTrue()
            ->and()
            ->when($this->commandBus()->dispatch($command))
            ->then()
                ->boolean($repository->isEmpty())
                    ->isFalse()
                ->object($repository->get($venueId))
                    ->isInstanceOf(ReadModelInterface::class)
        ;
    }
}
