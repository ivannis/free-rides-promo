<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Application\Tests\Units\Venue\Command;

use FreeRides\Core\Domain\Tests\Units\Command\CommandTestTrait;
use FreeRides\Location\Application\Tests\Units\TestCase;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * CreateVenueCommandTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateVenueCommandTests extends TestCase
{
    use CommandTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getArguments()
    {
        return array(
            VenueId::next()->toNative(),
            'Sagrada Familia',
            'Carrer de Mallorca, 401, Barcelona'
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function validatorProvider()
    {
        return array(
            array(
                'assert' => false,
                'arguments' => array(
                    '123',
                    'Sagrada Familia',
                    'Carrer de Mallorca, 401, Barcelona'
                ),
            ),
            array(
                'assert' => false,
                'arguments' => array(
                    VenueId::next()->toNative(),
                    345,
                    'Carrer de Mallorca, 401, Barcelona'
                ),
            ),
            array(
                'assert' => false,
                'arguments' => array(
                    VenueId::next()->toNative(),
                    'Sagrada Familia',
                    array()
                ),
            ),
        );
    }
}
