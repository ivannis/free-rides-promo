<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Address\Service;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Address\Service\AddressGeocoderInterface;
use FreeRides\Location\Domain\Address\Address;
use FreeRides\Location\Domain\Address\AddressId;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressRequest;
use Ivory\GoogleMap\Service\Geocoder\Response\GeocoderResponse;

/**
 * GoogleAddressGeocoder class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class GoogleAddressGeocoder implements AddressGeocoderInterface
{
    use GoogleHttpClientTrait;

    /**
     * @var GeocoderService
     */
    protected $geocoder;

    /**
     * GoogleAddressGeocoder constructor.
     *
     * @param string $googleMapKey
     * @param string $cacheDirectory
     */
    public function __construct($googleMapKey, $cacheDirectory)
    {
        $this->geocoder = new GeocoderService($this->getClient($cacheDirectory), new GuzzleMessageFactory());
        $this->geocoder->setKey($googleMapKey);
    }

    /**
     * {@inheritdoc}
     */
    public function geocode(StringLiteral $address)
    {
        $request = new GeocoderAddressRequest($address->toNative());
        $response = $this->attemptRequest($request);

        if ($response !== null) {
            foreach ($response->getResults() as $result) {
                // This can be improved, but for now we are goin to take the first result
                $location = $result->getGeometry()->getLocation();

                return new Address(
                    AddressId::fromNative($result->getPlaceId()),
                    StringLiteral::fromNative($result->getFormattedAddress()),
                    Coordinate::fromLatLng($location->getLatitude(), $location->getLongitude())
                );
            }
        }

        return null;
    }

    /**
     * @param GeocoderAddressRequest $request
     *
     * @return GeocoderResponse|null
     */
    private function doRequest(GeocoderAddressRequest $request)
    {
        return $this->geocoder->geocode($request);
    }
}
