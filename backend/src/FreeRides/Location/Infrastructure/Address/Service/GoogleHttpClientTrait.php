<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Address\Service;

use Http\Adapter\Guzzle6\Client;
use Http\Client\Common\Plugin\CachePlugin;
use Http\Client\Common\Plugin\ErrorPlugin as HttpErrorPlugin;
use Http\Client\Common\Plugin\RetryPlugin;
use Http\Client\Common\PluginClient;
use Http\Message\StreamFactory\GuzzleStreamFactory;
use Ivory\GoogleMap\Service\Geocoder\Response\GeocoderStatus;
use Ivory\GoogleMap\Service\RequestInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * GoogleHttpClient trait.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
trait GoogleHttpClientTrait
{
    /**
     * @param string $cacheDirectory
     *
     * @return PluginClient
     */
    public function getClient($cacheDirectory)
    {
        if (!file_exists($cacheDirectory)) {
            mkdir($cacheDirectory);
        }

        return new PluginClient(new Client(), [
            new RetryPlugin(),
            new HttpErrorPlugin(),
            new CachePlugin(
                new FilesystemAdapter('', 0, $cacheDirectory),
                new GuzzleStreamFactory(),
                [
                    'cache_lifetime'        => null,
                    'default_ttl'           => null,
                    'respect_cache_headers' => false,
                ]
            ),
        ]);
    }

    /**
     * @param RequestInterface $request
     *
     * @return Response|null
     */
    private function attemptRequest(RequestInterface $request)
    {
        $attempts = 0; $success = false;
        while ($success !== true && $attempts < 3) {
            $response = $this->doRequest($request);
            $attempts += 1;

            if ($response->getStatus() == GeocoderStatus::OVER_QUERY_LIMIT) {
                sleep(2);
            }

            $success = true;
        }

        if ($response->getStatus() == GeocoderStatus::OK) {
            return $response;
        }

        // Send an alert as this means that the daily limit has been reached
        print_r("Daily limit has been reached");

        return null;
    }
}
