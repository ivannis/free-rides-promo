<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Address\Service;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Application\Address\Service\DirectionsGeocoderInterface;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\GoogleMap\Service\Base\Location\AddressLocation;
use Ivory\GoogleMap\Service\Direction\DirectionService;
use Ivory\GoogleMap\Service\Direction\Request\DirectionRequest;
use Ivory\GoogleMap\Service\Direction\Response\DirectionResponse;

/**
 * GoogleDirectionsGeocoder class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class GoogleDirectionsGeocoder implements DirectionsGeocoderInterface
{
    use GoogleHttpClientTrait;

    /**
     * @var DirectionService
     */
    protected $geocoder;

    /**
     * GoogleDirectionsGeocoder constructor.
     *
     * @param string $googleMapKey
     * @param string $cacheDirectory
     */
    public function __construct($googleMapKey, $cacheDirectory)
    {
        $this->geocoder = new DirectionService($this->getClient($cacheDirectory), new GuzzleMessageFactory());
        $this->geocoder->setKey($googleMapKey);
    }

    /**
     * {@inheritdoc}
     */
    public function directions(Coordinate $origin, Coordinate $destination)
    {
        $request = new DirectionRequest(
            new AddressLocation(
                sprintf('%s,%s', $origin->latitude()->toNative(), $origin->longitude()->toNative())
            ),
            new AddressLocation(
                sprintf('%s,%s', $destination->latitude()->toNative(), $destination->longitude()->toNative())
            )
        );

        /** @var DirectionResponse $response */
        $response = $this->attemptRequest($request);
        if ($response !== null) {
            foreach ($response->getRoutes() as $result) {
                // This can be improved, but for now we are goin to take the first result
                return StringLiteral::fromNative($result->getOverviewPolyline()->getValue());
            }
        }

        return null;
    }

    /**
     * @param DirectionRequest $request
     *
     * @return DirectionResponse|null
     */
    private function doRequest(DirectionRequest $request)
    {
        return $this->geocoder->route($request);
    }
}
