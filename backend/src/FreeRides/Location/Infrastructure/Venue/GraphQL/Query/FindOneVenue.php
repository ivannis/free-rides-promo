<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Venue\GraphQL\Query;

use FreeRides\Location\Application\Venue\Controller\VenueReadController;
use FreeRides\Location\Application\Venue\ReadModel\Venue;
use FreeRides\Location\Infrastructure\Venue\GraphQL\VenueType;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * FindOneVenue class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindOneVenue extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('venueId', new NonNullType(new StringType()))
        ;
    }

    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Venue
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var VenueReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.venue');

        return $controller->findOneByIdAction($args['venueId']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'venue';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new VenueType();
    }
}
