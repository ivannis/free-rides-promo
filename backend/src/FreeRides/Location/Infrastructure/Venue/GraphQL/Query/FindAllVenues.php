<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Venue\GraphQL\Query;

use FreeRides\Location\Application\Venue\Controller\VenueReadController;
use FreeRides\Location\Infrastructure\Venue\GraphQL\VenueType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;

/**
 * FindAllVenues class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllVenues extends AbstractField
{
    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return mixed
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var VenueReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.venue');

        return $controller->findAllAction();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'venues';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new ListType(new VenueType());
    }
}
