<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Infrastructure\Venue\GraphQL;

use FreeRides\Core\Infrastructure\GraphQL\Type\CoordinateType;
use FreeRides\Location\Application\Venue\ReadModel\Venue;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * VenueType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (Venue $venue) {
                    return $venue->id()->toNative();
                },
            ])
            ->addField('name', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Venue $venue) {
                    return $venue->name()->toNative();
                },
            ])
            ->addField('address', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Venue $venue) {
                    return $venue->formattedAddress()->toNative();
                },
            ])
            ->addField('coordinate', [
                'type' => new NonNullType(new CoordinateType()),
                'resolve' => function (Venue $venue) {
                    return $venue->coordinate();
                },
            ])
        ;
    }
}
