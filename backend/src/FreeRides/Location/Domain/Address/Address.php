<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Domain\Address;

use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\StringLiteral;

/**
 * Address class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class Address extends Entity
{
    /**
     * @var StringLiteral
     */
    protected $formattedAddress;

    /**
     * @var Coordinate
     */
    protected $coordinate;

    /**
     * Address constructor.
     *
     * @param AddressId     $addressId
     * @param StringLiteral $formattedAddress
     * @param Coordinate    $coordinate
     */
    public function __construct(
        AddressId $addressId,
        StringLiteral $formattedAddress,
        Coordinate $coordinate
    ) {
        parent::__construct($addressId);

        $this->formattedAddress = $formattedAddress;
        $this->coordinate = $coordinate;
    }

    /**
     * @return AddressId
     */
    public function addressId()
    {
        return $this->id;
    }

    /**
     * @return StringLiteral
     */
    public function formattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * @return Coordinate
     */
    public function coordinate()
    {
        return $this->coordinate;
    }
}
