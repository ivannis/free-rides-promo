<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Domain\Address;

use Cubiche\Domain\Identity\StringId;

/**
 * AddressId class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class AddressId extends StringId
{
}
