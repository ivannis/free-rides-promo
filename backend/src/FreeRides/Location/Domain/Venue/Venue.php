<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Domain\Venue;

use Cubiche\Domain\EventSourcing\AggregateRoot;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Domain\Address\Address;
use FreeRides\Location\Domain\Address\AddressId;
use FreeRides\Location\Domain\Venue\Event\VenueWasCreated;

/**
 * Venue class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Venue extends AggregateRoot
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var Address
     */
    protected $address;

    /**
     * Venue constructor.
     *
     * @param VenueId       $venueId
     * @param StringLiteral $name
     * @param AddressId     $addressId
     * @param StringLiteral $formattedAddress
     * @param Coordinate    $coordinate
     */
    public function __construct(
        VenueId $venueId,
        StringLiteral $name,
        AddressId $addressId,
        StringLiteral $formattedAddress,
        Coordinate $coordinate
    ) {
        parent::__construct($venueId);

        $this->recordAndApplyEvent(
            new VenueWasCreated(
                $venueId,
                $name,
                new Address(
                    $addressId,
                    $formattedAddress,
                    $coordinate
                )
            )
        );
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return Address
     */
    public function address()
    {
        return $this->address;
    }

    /**
     * @param VenueWasCreated $event
     */
    protected function applyVenueWasCreated(VenueWasCreated $event)
    {
        $this->name = $event->name();
        $this->address = $event->address();
    }
}
