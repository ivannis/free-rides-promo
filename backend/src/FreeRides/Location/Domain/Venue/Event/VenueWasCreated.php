<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Domain\Venue\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Domain\Address\Address;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueWasCreated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueWasCreated extends DomainEvent
{
    /**
     * @var StringLiteral;
     */
    protected $name;

    /**
     * @var Address
     */
    protected $address;

    /**
     * VenueWasCreated constructor.
     *
     * @param VenueId       $venueId
     * @param StringLiteral $name
     * @param Address       $address
     */
    public function __construct(
        VenueId $venueId,
        StringLiteral $name,
        Address $address
    ) {
        parent::__construct($venueId);

        $this->name = $name;
        $this->address = $address;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->aggregateId();
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return Address
     */
    public function address()
    {
        return $this->address;
    }
}
