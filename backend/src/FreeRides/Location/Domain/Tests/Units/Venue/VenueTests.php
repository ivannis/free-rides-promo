<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Location\Domain\Tests\Units\Venue;

use Cubiche\Domain\EventSourcing\AggregateRootInterface;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Location\Domain\Address\AddressId;
use FreeRides\Location\Domain\Tests\Units\TestCase;
use FreeRides\Location\Domain\Venue\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * VenueTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class VenueTests extends TestCase
{
    /**
     * @return Venue
     */
    protected function createVenue()
    {
          return new Venue(
              VenueId::next(),
              StringLiteral::fromNative('Sagrada Familia'),
              AddressId::fromNative('ChIJ0YdHm-eipBIRZgXp5nBJQtg'),
              StringLiteral::fromNative('Carrer de Mallorca, 401, Barcelona'),
              Coordinate::fromLatLng(41.404005, 2.174893)
          );
    }

    /**
     * Test class.
     */
    public function testClass()
    {
        $this
            ->testedClass
                ->implements(AggregateRootInterface::class)
        ;
    }

    /**
     * Test Name method.
     */
    public function testName()
    {
        $this
            ->given($venue = $this->createVenue())
            ->then()
                ->string($venue->name()->toNative())
                    ->isEqualTo('Sagrada Familia')
        ;
    }

    /**
     * Test Address method.
     */
    public function testAddress()
    {
        $this
            ->given($venue = $this->createVenue())
            ->then()
                ->string($venue->address()->formattedAddress()->toNative())
                    ->isEqualTo('Carrer de Mallorca, 401, Barcelona')
                ->float($venue->address()->coordinate()->latitude()->toNative())
                    ->isEqualTo(41.404005)
                ->float($venue->address()->coordinate()->longitude()->toNative())
                    ->isEqualTo(2.174893)
        ;
    }
}
