<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\ReadModel;

use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Events\Application\Event\ReadModel\Query\FindAllEvents;
use FreeRides\Events\Application\Event\ReadModel\Query\FindOneEventById;
use FreeRides\Events\Domain\Event\EventId;

/**
 * EventQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * CurrencyQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllEvents $query
     *
     * @return Event[]
     */
    public function findAllEvents(FindAllEvents $query)
    {
        return $this->repository->getIterator();
    }

    /**
     * @param FindOneEventById $query
     *
     * @return Event|null
     */
    public function findOneEventById(FindOneEventById $query)
    {
        return $this->repository->findOne(
            Criteria::property('id')->eq(EventId::fromNative($query->eventId()))
        );
    }
}
