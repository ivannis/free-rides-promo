<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\ReadModel\Projection;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Event\ReadModel\Event;
use FreeRides\Events\Domain\Event\Event\EventPromoCodeWasAdded;
use FreeRides\Events\Domain\Event\Event\EventWasCreated;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Application\Venue\ReadModel\Venue;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * EventProjector class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventProjector implements DomainEventSubscriberInterface
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * @var QueryRepositoryInterface
     */
    protected $venueRepository;

    /**
     * Projector constructor.
     *
     * @param QueryRepositoryInterface $repository
     * @param QueryRepositoryInterface $venueRepository
     */
    public function __construct(QueryRepositoryInterface $repository, QueryRepositoryInterface $venueRepository)
    {
        $this->repository = $repository;
        $this->venueRepository = $venueRepository;
    }

    /**
     * @param EventWasCreated $event
     */
    public function whenEventWasCreated(EventWasCreated $event)
    {
        $venue = $this->findVenueOr404($event->venueId());

        $aggregate = new Event(
            $event->eventId(),
            $event->venueId(),
            $event->name(),
            $venue->formattedAddress(),
            $venue->coordinate()
        );

        $this->repository->persist($aggregate);
    }

    /**
     * @param EventPromoCodeWasAdded $event
     */
    public function whenEventPromoCodeWasAdded(EventPromoCodeWasAdded $event)
    {
        $aggregate = $this->findOr404($event->eventId());
        $aggregate->addPromoCode($event->promoCodeId());

        $this->repository->persist($aggregate);
    }

    /**
     * @param EventId $eventId
     *
     * @return Event
     */
    private function findOr404(EventId $eventId)
    {
        $event = $this->repository->get($eventId);
        if ($event === null) {
            throw new NotFoundException(sprintf(
                'There is no event with id: %s',
                $event
            ));
        }

        return $event;
    }

    /**
     * @param VenueId $venueId
     *
     * @return Venue
     */
    private function findVenueOr404(VenueId $venueId)
    {
        $venue = $this->venueRepository->get($venueId);
        if ($venue === null) {
            throw new NotFoundException(sprintf(
                'There is no venue with id: %s',
                $venue
            ));
        }

        return $venue;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            EventWasCreated::class => array('whenEventWasCreated'),
            EventPromoCodeWasAdded::class => array('whenEventPromoCodeWasAdded'),
        );
    }
}
