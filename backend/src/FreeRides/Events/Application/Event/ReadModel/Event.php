<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\ReadModel;

use Cubiche\Core\Collections\ArrayCollection\ArrayHashMap;
use Cubiche\Core\Collections\CollectionInterface;
use Cubiche\Domain\EventSourcing\ReadModelInterface;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * Event class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Event extends Entity implements ReadModelInterface
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var VenueId
     */
    protected $venueId;

    /**
     * @var StringLiteral
     */
    protected $formattedAddress;

    /**
     * @var Coordinate
     */
    protected $coordinate;

    /**
     * @var ArrayHashMap|PromoCodeId[]
     */
    protected $promoCodes;

    /**
     * Event constructor.
     *
     * @param EventId       $eventId
     * @param VenueId       $venueId
     * @param StringLiteral $name
     * @param StringLiteral $formattedAddress
     * @param Coordinate    $coordinate
     */
    public function __construct(
        EventId $eventId,
        VenueId $venueId,
        StringLiteral $name,
        StringLiteral $formattedAddress,
        Coordinate $coordinate
    ) {
        parent::__construct($eventId);

        $this->name = $name;
        $this->venueId = $venueId;
        $this->formattedAddress = $formattedAddress;
        $this->coordinate = $coordinate;
        $this->promoCodes = new ArrayHashMap();
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->id;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return StringLiteral
     */
    public function formattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * @return Coordinate
     */
    public function coordinate()
    {
        return $this->coordinate;
    }

    /**
     * @return CollectionInterface|PromoCodeId[]
     */
    public function promoCodes()
    {
        return $this->promoCodes->values();
    }

    /**
     * @param PromoCodeId $promoCodeId
     */
    public function addPromoCode(PromoCodeId $promoCodeId)
    {
        $this->promoCodes->set($promoCodeId->toNative(), $promoCodeId);
    }

    /**
     * @param PromoCodeId $promoCodeId
     */
    public function removePromoCode(PromoCodeId $promoCodeId)
    {
        $this->promoCodes->removeAt($promoCodeId->toNative());
    }
}
