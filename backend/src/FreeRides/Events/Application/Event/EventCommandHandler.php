<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event;

use Cubiche\Domain\Repository\RepositoryInterface;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Event\Command\AddPromoCodeToAnEventCommand;
use FreeRides\Events\Application\Event\Command\CreateEventCommand;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\Event;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * EventCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventCommandHandler
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * EventCommandHandler constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateEventCommand $command
     */
    public function createEvent(CreateEventCommand $command)
    {
        $event = new Event(
            EventId::fromNative($command->eventId()),
            StringLiteral::fromNative($command->name()),
            VenueId::fromNative($command->venueId())
        );

        $this->repository->persist($event);
    }

    /**
     * @param AddPromoCodeToAnEventCommand $command
     */
    public function addPromoCodeToAnEvent(AddPromoCodeToAnEventCommand $command)
    {
        $event = $this->findOr404($command->eventId());
        $event->addPromoCode(PromoCodeId::fromNative($command->promoCodeId()));

        $this->repository->persist($event);
    }

    /**
     * @param string $eventId
     *
     * @return Event
     */
    private function findOr404($eventId)
    {
        /** @var Event $event */
        $event = $this->repository->get(EventId::fromNative($eventId));
        if ($event === null) {
            throw new NotFoundException(sprintf(
                'There is no event with id: %s',
                $eventId
            ));
        }

        return $event;
    }
}
