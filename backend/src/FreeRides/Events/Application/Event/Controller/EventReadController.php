<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\Controller;

use FreeRides\Core\Application\Controller\QueryController;
use FreeRides\Events\Application\Event\ReadModel\Query\FindAllEvents;
use FreeRides\Events\Application\Event\ReadModel\Query\FindOneEventById;
use FreeRides\Events\Application\Event\ReadModel\Event;

/**
 * EventReadController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventReadController extends QueryController
{
    /**
     * @return Event[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(
            new FindAllEvents()
        );
    }

    /**
     * @param string $eventId
     *
     * @return Event
     */
    public function findOneByIdAction($eventId)
    {
        return $this->queryBus()->dispatch(
            new FindOneEventById($eventId)
        );
    }
}
