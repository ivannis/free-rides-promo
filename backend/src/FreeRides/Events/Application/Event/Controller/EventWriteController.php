<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\Controller;

use Cubiche\Core\Cqrs\Command\CommandBus;
use FreeRides\Core\Application\Controller\CommandController;
use FreeRides\Events\Application\Coupon\Controller\PromoCodeWriteController;
use FreeRides\Events\Application\Event\Command\AddPromoCodeToAnEventCommand;
use FreeRides\Events\Application\Event\Command\CreateEventCommand;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Application\Venue\Controller\VenueWriteController;

/**
 * EventWriteController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventWriteController extends CommandController
{
    /**
     * @var VenueWriteController
     */
    protected $venueController;

    /**
     * @var PromoCodeWriteController
     */
    protected $promoCodeController;

    /**
     * EventWriteController constructor.
     *
     * @param CommandBus               $commandBus
     * @param VenueWriteController     $venueController
     * @param PromoCodeWriteController $promoCodeController
     */
    public function __construct(
        CommandBus $commandBus,
        VenueWriteController $venueController,
        PromoCodeWriteController $promoCodeController
    ) {
        parent::__construct($commandBus);

        $this->venueController = $venueController;
        $this->promoCodeController = $promoCodeController;
    }

    /**
     * @param string $name
     * @param string $venueName
     * @param string $venueAddress
     *
     * @return string
     */
    public function createAction($name, $venueName, $venueAddress)
    {
        $venueId = $this->venueController->createAction($venueName, $venueAddress);
        $eventId = EventId::next()->toNative();

        $this->commandBus()->dispatch(
            new CreateEventCommand($eventId, $name, $venueId)
        );

        return $eventId;
    }

    /**
     * @param string      $eventId
     * @param int         $radius
     * @param int         $amountOfRide
     * @param string|null $expiredAt
     *
     * @return string
     */
    public function generatePromoCodeAction($eventId, $radius = 20, $amountOfRide = 1, $expiredAt = null)
    {
        $promoCodeId = $this->promoCodeController->createAction($eventId, $radius, $amountOfRide, $expiredAt);
        $this->commandBus()->dispatch(
            new AddPromoCodeToAnEventCommand($eventId, $promoCodeId)
        );

        return $promoCodeId;
    }
}
