<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * CreateEventCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateEventCommand extends Command
{
    /**
     * @var string
     */
    protected $eventId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $venueId;

    /**
     * CreateEventCommand constructor.
     *
     * @param string $eventId
     * @param string $name
     * @param string $venueId
     */
    public function __construct($eventId, $name, $venueId)
    {
        $this->eventId = $eventId;
        $this->name = $name;
        $this->venueId = $venueId;
    }

    /**
     * @return string
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('eventId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('name', Assertion::string()->notBlank());
        $classMetadata->addPropertyConstraint('venueId', Assertion::string()->notBlank());
    }
}
