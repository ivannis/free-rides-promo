<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Event\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * AddPromoCodeToAnEventCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class AddPromoCodeToAnEventCommand extends Command
{
    /**
     * @var string
     */
    protected $eventId;

    /**
     * @var string
     */
    protected $promoCodeId;

    /**
     * AddPromoCodeToAnEventCommand constructor.
     *
     * @param string $eventId
     * @param string $promoCodeId
     */
    public function __construct($eventId, $promoCodeId)
    {
        $this->eventId = $eventId;
        $this->promoCodeId = $promoCodeId;
    }

    /**
     * @return string
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return string
     */
    public function promoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('eventId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('promoCodeId', Assertion::string()->notBlank());
    }
}
