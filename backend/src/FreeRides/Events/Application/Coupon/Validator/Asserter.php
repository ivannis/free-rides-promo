<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Validator;

use Cubiche\Core\Cqrs\Query\QueryBus;
use Cubiche\Core\Validator\Assert;
use Cubiche\Core\Validator\Exception\InvalidArgumentException;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\Geolocation\DistanceUnit;
use FreeRides\Events\Application\Coupon\ReadModel\PromoCode;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindOnePromoCodeById;

/**
 * Asserter class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Asserter
{
    /**
     * @var QueryBus
     */
    protected $queryBus;

    /**
     * Asserter constructor.
     *
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param string               $value
     * @param Coordinate           $origin
     * @param Coordinate           $destination
     * @param string|callable|null $message
     * @param string|null          $propertyPath
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function validPromoCode(
        $value,
        Coordinate $origin,
        Coordinate $destination,
        $message = null,
        $propertyPath = null
    ){
        $promoCode = $this->promoCodeOrException($value, $message, $propertyPath);

        $venueCoordinate = $promoCode->coordinate();
        $radius = $promoCode->radius()->toNative();

        $originDistance = $this->distanceInKM($origin, $venueCoordinate);
        $destinationDistance = $this->distanceInKM($destination, $venueCoordinate);

        if ($originDistance > $radius) {
            $message = sprintf(
                Assert::generateMessage(
                    'You should take the ride within a radius of %s km. You are %s km from the event venue.'
                ),
                Assert::stringify($radius),
                Assert::stringify(round($originDistance, 2))
            );

            throw Assert::createException($value, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        if ($destinationDistance > $radius) {
            $message = sprintf(
                Assert::generateMessage(
                    'You should take the ride within a radius of %s km. Your destination is %s km from the event venue.'
                ),
                Assert::stringify($radius),
                Assert::stringify(round($destinationDistance, 2))
            );

            throw Assert::createException($value, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        return true;
        if ($originDistance <= $radius || $destinationDistance <= $radius) {
            return true;
        }

        $message = sprintf(
            Assert::generateMessage(
                'Too far from the event venue. You should take the ride in a radius of %s km.'
            ),
            Assert::stringify($radius)
        );

        throw Assert::createException($value, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
    }

    /**
     * @param string               $promoCodeId
     * @param string|callable|null $message
     * @param string|null          $propertyPath
     *
     * @return PromoCode|null
     */
    private function promoCodeOrException($promoCodeId, $message = null, $propertyPath = null)
    {
        /** @var PromoCode $promoCode */
        $promoCode = $this->queryBus->dispatch(new FindOnePromoCodeById($promoCodeId));
        if ($promoCode === null) {
            $message = sprintf(
                Assert::generateMessage('Promo code "%s" does not exist.'),
                Assert::stringify($promoCodeId)
            );

            throw Assert::createException($promoCodeId, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        if ($promoCode->isExpired()) {
            $message = sprintf(
                Assert::generateMessage('Coupon code %s has expired.'),
                Assert::stringify($promoCodeId)
            );

            throw Assert::createException($promoCodeId, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        if (!$promoCode->isActive()) {
            $message = sprintf(
                Assert::generateMessage('Coupon code %s is not active.'),
                Assert::stringify($promoCodeId)
            );

            throw Assert::createException($promoCodeId, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        if ($promoCode->amountOfRide()->isZero() || $promoCode->amountOfRide()->isNegative()) {
            $message = sprintf(
                Assert::generateMessage('Coupon code %s sold out the number of rides.'),
                Assert::stringify($promoCodeId)
            );

            throw Assert::createException($promoCodeId, $message, Assert::INVALID_CUSTOM_ASSERT, $propertyPath);
        }

        return $promoCode;
    }

    /**
     * @param Coordinate $origin
     * @param Coordinate $destination
     *
     * @return float
     */
    private function distanceInKM(Coordinate $origin, Coordinate $destination)
    {
        return $origin->distance($destination, DistanceUnit::KILOMETER())->value()->toNative();
    }
}
