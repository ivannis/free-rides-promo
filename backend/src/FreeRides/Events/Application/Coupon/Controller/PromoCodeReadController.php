<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Controller;

use Cubiche\Core\Cqrs\Query\QueryBus;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Validator;
use FreeRides\Core\Application\Controller\QueryController;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Coupon\ReadModel\PromoCode;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindActivesPromoCodes;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindAllPromoCodes;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindOnePromoCodeById;
use FreeRides\Location\Application\Address\Controller\AddressReadController;
use FreeRides\Location\Domain\Address\Address;

/**
 * PromoCodeReadController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeReadController extends QueryController
{
    /**
     * @var AddressReadController
     */
    protected $addressController;

    /**
     * PromoCodeReadController constructor.
     *
     * @param QueryBus              $queryBus
     * @param AddressReadController $addressController
     */
    public function __construct(QueryBus $queryBus, AddressReadController $addressController)
    {
        parent::__construct($queryBus);

        $this->addressController = $addressController;
    }

    /**
     * @return PromoCode[]
     */
    public function findAllAction()
    {
        return $this->queryBus()->dispatch(
            new FindAllPromoCodes()
        );
    }

    /**
     * @return PromoCode[]
     */
    public function findActivesAction()
    {
        return $this->queryBus()->dispatch(
            new FindActivesPromoCodes()
        );
    }

    /**
     * @param string $promoCodeId
     *
     * @return PromoCode|null
     */
    public function findOneByIdAction($promoCodeId)
    {
        return $this->queryBus()->dispatch(
            new FindOnePromoCodeById($promoCodeId)
        );
    }

    /**
     * @param string $promoCodeId
     * @param string $origin
     * @param string $destination
     *
     * @return bool
     */
    public function validityAction($promoCodeId, $origin, $destination)
    {
        $originAddress = $this->findAddressOr404($origin);
        $destinationAddress = $this->findAddressOr404($destination);

        Validator::assert(
            $promoCodeId,
            Assertion::validPromoCode(
                $originAddress->coordinate(),
                $destinationAddress->coordinate(),
                null,
                'promoCodeId'
            )
        );

        return $this->addressController->directionsAction(
            $originAddress->coordinate()->latitude()->toNative(),
            $originAddress->coordinate()->longitude()->toNative(),
            $destinationAddress->coordinate()->latitude()->toNative(),
            $destinationAddress->coordinate()->longitude()->toNative()
        );
    }

    /**
     * @param string $street
     *
     * @return Address
     */
    private function findAddressOr404($street)
    {
        $address = $this->addressController->geocodeAction($street);
        if ($address === null) {
            throw new NotFoundException(sprintf(
                'There is no geocode information for the given address "%s"',
                $address
            ));
        }

        return $address;
    }
}
