<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Controller;

use Cubiche\Core\Cqrs\Command\CommandBus;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Validator;
use FreeRides\Core\Application\Controller\CommandController;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Coupon\Command\ChangePromoCodeRadiusCommand;
use FreeRides\Events\Application\Coupon\Command\CreatePromoCodeCommand;
use FreeRides\Events\Application\Coupon\Command\DeactivatePromoCodeCommand;
use FreeRides\Events\Application\Coupon\Command\UsePromoCodeCommand;
use FreeRides\Events\Application\Coupon\Service\PromoCodeGeneratorInterface;
use FreeRides\Location\Application\Address\Controller\AddressReadController;

/**
 * PromoCodeWriteController class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeWriteController extends CommandController
{
    /**
     * @var AddressReadController
     */
    protected $addressController;

    /**
     * @var PromoCodeGeneratorInterface
     */
    protected $promoCodeGenerator;

    /**
     * EventWriteController constructor.
     *
     * @param CommandBus                  $commandBus
     * @param AddressReadController       $addressController
     * @param PromoCodeGeneratorInterface $promoCodeGenerator
     */
    public function __construct(
        CommandBus $commandBus,
        AddressReadController $addressController,
        PromoCodeGeneratorInterface $promoCodeGenerator
    ) {
        parent::__construct($commandBus);

        $this->addressController = $addressController;
        $this->promoCodeGenerator = $promoCodeGenerator;
    }

    /**
     * @param string      $eventId
     * @param int         $radius
     * @param int         $amountOfRide
     * @param string|null $expiredAt
     *
     * @return string
     */
    public function createAction($eventId, $radius = 20, $amountOfRide = 1, $expiredAt = null)
    {
        $promoCodeId = $this->promoCodeGenerator->next()->toNative();
        $this->commandBus()->dispatch(
            new CreatePromoCodeCommand($promoCodeId, $eventId, $radius, $amountOfRide, $expiredAt)
        );

        return $promoCodeId;
    }

    /**
     * @param string $promoCodeId
     *
     * @return bool
     */
    public function deactivateAction($promoCodeId)
    {
        $this->commandBus()->dispatch(
            new DeactivatePromoCodeCommand($promoCodeId)
        );

        return true;
    }

    /**
     * @param string $promoCodeId
     * @param int    $radius
     *
     * @return bool
     */
    public function changeRadiusAction($promoCodeId, $radius)
    {
        $this->commandBus()->dispatch(
            new ChangePromoCodeRadiusCommand($promoCodeId, $radius)
        );

        return true;
    }

    /**
     * @param string $promoCodeId
     * @param string $origin
     * @param string $destination
     *
     * @return bool
     */
    public function takeARideAction($promoCodeId, $origin, $destination)
    {
        $originAddress = $this->findAddressOr404($origin);
        $destinationAddress = $this->findAddressOr404($destination);

        Validator::assert(
            $promoCodeId,
            Assertion::validPromoCode(
                $originAddress->coordinate(),
                $destinationAddress->coordinate(),
                null,
                'promoCodeId'
            )
        );

        $this->commandBus()->dispatch(
            new UsePromoCodeCommand($promoCodeId)
        );

        return true;
    }

    /**
     * @param string $street
     *
     * @return Address
     */
    private function findAddressOr404($street)
    {
        $address = $this->addressController->geocodeAction($street);
        if ($address === null) {
            throw new NotFoundException(sprintf(
                'There is no geocode information for the given address "%s"',
                $address
            ));
        }

        return $address;
    }
}
