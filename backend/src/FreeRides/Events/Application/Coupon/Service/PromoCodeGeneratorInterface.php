<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Service;

use FreeRides\Events\Domain\Coupon\PromoCodeId;

/**
 * PromoCodeGenerator interface.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
interface PromoCodeGeneratorInterface
{
    /**
     * @return PromoCodeId
     */
    public function next();
}
