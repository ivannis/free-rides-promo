<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon;

use Cubiche\Domain\Repository\RepositoryInterface;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Coupon\Command\ChangePromoCodeRadiusCommand;
use FreeRides\Events\Application\Coupon\Command\CreatePromoCodeCommand;
use FreeRides\Events\Application\Coupon\Command\DeactivatePromoCodeCommand;
use FreeRides\Events\Application\Coupon\Command\UsePromoCodeCommand;
use FreeRides\Events\Domain\Coupon\PromoCode;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;

/**
 * PromoCodeCommandHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeCommandHandler
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * PromoCodeCommandHandler constructor.
     *
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreatePromoCodeCommand $command
     */
    public function createPromoCode(CreatePromoCodeCommand $command)
    {
        $promoCode = new PromoCode(
            PromoCodeId::fromNative($command->promoCodeId()),
            EventId::fromNative($command->eventId()),
            Integer::fromNative($command->radius()),
            Integer::fromNative($command->amountOfRide()),
            $this->expiredAtToDateTime($command->expiredAt())
        );

        $this->repository->persist($promoCode);
    }

    /**
     * @param ChangePromoCodeRadiusCommand $command
     */
    public function changePromoCodeRadius(ChangePromoCodeRadiusCommand $command)
    {
        $promoCode = $this->findOr404($command->promoCodeId());
        $promoCode->changeRadius(Integer::fromNative($command->radius()));

        $this->repository->persist($promoCode);
    }

    /**
     * @param DeactivatePromoCodeCommand $command
     */
    public function deactivatePromoCode(DeactivatePromoCodeCommand $command)
    {
        $promoCode = $this->findOr404($command->promoCodeId());
        $promoCode->deactivate();

        $this->repository->persist($promoCode);
    }

    /**
     * @param UsePromoCodeCommand $command
     */
    public function usePromoCode(UsePromoCodeCommand $command)
    {
        $promoCode = $this->findOr404($command->promoCodeId());
        $promoCode->takeARide();

        $this->repository->persist($promoCode);
    }

    /**
     * @param string $expiredAt
     *
     * @return DateTime
     */
    private function expiredAtToDateTime($expiredAt)
    {
        if (is_string($expiredAt)) {
            return DateTime::fromNative(date_create()->modify($expiredAt));
        }

        if (is_int($expiredAt)) {
            return DateTime::fromTimestamp($expiredAt);
        }

        return null;
    }

    /**
     * @param string $promoCodeId
     *
     * @return PromoCode
     */
    private function findOr404($promoCodeId)
    {
        /** @var PromoCode $promoCode */
        $promoCode = $this->repository->get(PromoCodeId::fromNative($promoCodeId));
        if ($promoCode === null) {
            throw new NotFoundException(sprintf(
                'There is no promoCode with id: %s',
                $promoCodeId
            ));
        }

        return $promoCode;
    }
}
