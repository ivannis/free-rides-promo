<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * CreatePromoCodeCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreatePromoCodeCommand extends Command
{
    /**
     * @var string
     */
    protected $promoCodeId;

    /**
     * @var string
     */
    protected $eventId;

    /**
     * @var int
     */
    protected $radius;

    /**
     * @var int
     */
    protected $amountOfRide;

    /**
     * @var mixed
     */
    protected $expiredAt;

    /**
     * CreatePromoCodeCommand constructor.
     *
     * @param string $promoCodeId
     * @param string $eventId
     * @param int    $radius
     * @param int    $amountOfRide
     * @param mixed  $expiredAt
     */
    public function __construct($promoCodeId, $eventId, $radius, $amountOfRide, $expiredAt)
    {
        $this->promoCodeId = $promoCodeId;
        $this->eventId = $eventId;
        $this->radius = $radius;
        $this->amountOfRide = $amountOfRide;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return string
     */
    public function promoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * @return string
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return int
     */
    public function radius()
    {
        return $this->radius;
    }

    /**
     * @return int
     */
    public function amountOfRide()
    {
        return $this->amountOfRide;
    }

    /**
     * @return mixed
     */
    public function expiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('promoCodeId', Assertion::string()->notBlank());
        $classMetadata->addPropertyConstraint('eventId', Assertion::uuid()->notBlank());
        $classMetadata->addPropertyConstraint('radius', Assertion::integer()->notBlank());
        $classMetadata->addPropertyConstraint('amountOfRide', Assertion::integer()->notBlank());
    }
}
