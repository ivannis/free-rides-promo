<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * DeactivatePromoCodeCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class DeactivatePromoCodeCommand extends Command
{
    /**
     * @var string
     */
    protected $promoCodeId;

    /**
     * DeactivatePromoCodeCommand constructor.
     *
     * @param string $promoCodeId
     */
    public function __construct($promoCodeId)
    {
        $this->promoCodeId = $promoCodeId;
    }

    /**
     * @return string
     */
    public function promoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('promoCodeId', Assertion::string()->notBlank());
    }
}
