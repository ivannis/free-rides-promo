<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\Command;

use Cubiche\Core\Cqrs\Command\Command;
use Cubiche\Core\Validator\Assertion;
use Cubiche\Core\Validator\Mapping\ClassMetadata;

/**
 * ChangePromoCodeRadiusCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class ChangePromoCodeRadiusCommand extends Command
{
    /**
     * @var string
     */
    protected $promoCodeId;

    /**
     * @var int
     */
    protected $radius;

    /**
     * ChangePromoCodeRadiusCommand constructor.
     *
     * @param string $promoCodeId
     * @param int    $radius
     */
    public function __construct($promoCodeId, $radius)
    {
        $this->promoCodeId = $promoCodeId;
        $this->radius = $radius;
    }

    /**
     * @return string
     */
    public function promoCodeId()
    {
        return $this->promoCodeId;
    }

    /**
     * @return int
     */
    public function radius()
    {
        return $this->radius;
    }

    /**
     * {@inheritdoc}
     */
    public static function loadValidatorMetadata(ClassMetadata $classMetadata)
    {
        $classMetadata->addPropertyConstraint('promoCodeId', Assertion::string()->notBlank());
        $classMetadata->addPropertyConstraint('radius', Assertion::integer()->notBlank());
    }
}
