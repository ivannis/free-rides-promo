<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\ReadModel;

use Cubiche\Core\Specification\Criteria;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindActivesPromoCodes;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindAllPromoCodes;
use FreeRides\Events\Application\Coupon\ReadModel\Query\FindOnePromoCodeById;
use FreeRides\Events\Domain\Coupon\PromoCodeId;

/**
 * PromoCodeQueryHandler class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeQueryHandler
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * CurrencyQueryHandler constructor.
     *
     * @param QueryRepositoryInterface $repository
     */
    public function __construct(QueryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param FindAllPromoCodes $query
     *
     * @return PromoCode[]
     */
    public function findAllPromoCodes(FindAllPromoCodes $query)
    {
        return $this->repository->getIterator();
    }

    /**
     * @param FindActivesPromoCodes $query
     *
     * @return PromoCode[]
     */
    public function findActivesPromoCodes(FindActivesPromoCodes $query)
    {
        return $this->repository->find(
            Criteria::property('active')->isTrue()
        );
    }

    /**
     * @param FindOnePromoCodeById $query
     *
     * @return PromoCode|null
     */
    public function findOnePromoCodeById(FindOnePromoCodeById $query)
    {
        return $this->repository->findOne(
            Criteria::property('id')->eq(PromoCodeId::fromNative($query->promoCodeId()))
        );
    }
}
