<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\ReadModel\Projection;

use Cubiche\Domain\EventPublisher\DomainEventSubscriberInterface;
use Cubiche\Domain\Repository\QueryRepositoryInterface;
use FreeRides\Core\Domain\Exception\NotFoundException;
use FreeRides\Events\Application\Coupon\ReadModel\PromoCode;
use FreeRides\Events\Application\Event\ReadModel\Event;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeRadiusWasUpdated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasCreated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasDeactivated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasUsed;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;

/**
 * PromoCodeProjector class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeProjector implements DomainEventSubscriberInterface
{
    /**
     * @var QueryRepositoryInterface
     */
    protected $repository;

    /**
     * @var QueryRepositoryInterface
     */
    protected $eventRepository;

    /**
     * Projector constructor.
     *
     * @param QueryRepositoryInterface $repository
     * @param QueryRepositoryInterface $eventRepository
     */
    public function __construct(QueryRepositoryInterface $repository, QueryRepositoryInterface $eventRepository)
    {
        $this->repository = $repository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @param PromoCodeWasCreated $event
     */
    public function whenPromoCodeWasCreated(PromoCodeWasCreated $event)
    {
        $eventAggregate = $this->findEventOr404($event->eventId());

        $promoCode = new PromoCode(
            $event->promoCodeId(),
            $event->eventId(),
            $eventAggregate->venueId(),
            $event->radius(),
            $event->amountOfRide(),
            $eventAggregate->name(),
            $eventAggregate->formattedAddress(),
            $eventAggregate->coordinate(),
            $event->expiredAt()
        );

        $this->repository->persist($promoCode);
    }

    /**
     * @param PromoCodeRadiusWasUpdated $event
     */
    public function whenPromoCodeRadiusWasUpdated(PromoCodeRadiusWasUpdated $event)
    {
        $promoCode = $this->findOr404($event->promoCodeId());
        $promoCode->changeRadius($event->radius());

        $this->repository->persist($promoCode);
    }

    /**
     * @param PromoCodeWasUsed $event
     */
    public function whenPromoCodeWasUsed(PromoCodeWasUsed $event)
    {
        $promoCode = $this->findOr404($event->promoCodeId());
        $promoCode->decrementAmountOfRide();

        $this->repository->persist($promoCode);
    }

    /**
     * @param PromoCodeWasDeactivated $event
     */
    public function whenPromoCodeWasDeactivated(PromoCodeWasDeactivated $event)
    {
        $promoCode = $this->findOr404($event->promoCodeId());
        $promoCode->deactivate();

        $this->repository->persist($promoCode);
    }

    /**
     * @param PromoCodeId $promoCodeId
     *
     * @return PromoCode
     */
    private function findOr404(PromoCodeId $promoCodeId)
    {
        $promoCode = $this->repository->get($promoCodeId);
        if ($promoCode === null) {
            throw new NotFoundException(sprintf(
                'There is no promoCode with id: %s',
                $promoCode
            ));
        }

        return $promoCode;
    }

    /**
     * @param EventId $eventId
     *
     * @return Event
     */
    private function findEventOr404(EventId $eventId)
    {
        $event = $this->eventRepository->get($eventId);
        if ($event === null) {
            throw new NotFoundException(sprintf(
                'There is no event with id: %s',
                $event
            ));
        }

        return $event;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            PromoCodeWasCreated::class => array('whenPromoCodeWasCreated'),
            PromoCodeRadiusWasUpdated::class => array('whenPromoCodeRadiusWasUpdated'),
            PromoCodeWasUsed::class => array('whenPromoCodeWasUsed'),
            PromoCodeWasDeactivated::class => array('whenPromoCodeWasDeactivated'),
        );
    }
}
