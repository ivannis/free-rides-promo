<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\ReadModel\Query;

use Cubiche\Core\Cqrs\Query\Query;

/**
 * FindActivesPromoCodes class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindActivesPromoCodes extends Query
{
}
