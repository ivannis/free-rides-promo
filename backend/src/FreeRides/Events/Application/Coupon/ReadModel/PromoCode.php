<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Application\Coupon\ReadModel;

use Cubiche\Domain\EventSourcing\ReadModelInterface;
use Cubiche\Domain\Geolocation\Coordinate;
use Cubiche\Domain\Model\Entity;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * PromoCode class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCode extends Entity implements ReadModelInterface
{
    /**
     * @var EventId
     */
    protected $eventId;

    /**
     * @var VenueId
     */
    protected $venueId;

    /**
     * @var \Cubiche\Domain\System\Integer
     */
    protected $radius;

    /**
     * @var \Cubiche\Domain\System\Integer
     */
    protected $amountOfRide;

    /**
     * @var StringLiteral
     */
    protected $eventName;

    /**
     * @var StringLiteral
     */
    protected $venueAddress;

    /**
     * @var Coordinate
     */
    protected $coordinate;

    /**
     * @var boolean
     */
    protected $active;

    /**
     * @var DateTime
     */
    protected $expiredAt;

    /**
     * PromoCode constructor.
     *
     * @param PromoCodeId   $promoCodeId
     * @param EventId       $eventId
     * @param VenueId       $venueId
     * @param Integer       $radius
     * @param Integer       $amountOfRide
     * @param StringLiteral $eventName
     * @param StringLiteral $venueAddress
     * @param Coordinate    $coordinate
     * @param DateTime|null $expiredAt
     */
    public function __construct(
        PromoCodeId $promoCodeId,
        EventId $eventId,
        VenueId $venueId,
        Integer $radius,
        Integer $amountOfRide,
        StringLiteral $eventName,
        StringLiteral $venueAddress,
        Coordinate $coordinate,
        DateTime $expiredAt = null
    ) {
        parent::__construct($promoCodeId);

        $this->eventId = $eventId;
        $this->venueId = $venueId;
        $this->radius = $radius;
        $this->amountOfRide = $amountOfRide;
        $this->eventName = $eventName;
        $this->venueAddress = $venueAddress;
        $this->coordinate = $coordinate;
        $this->active = true;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->id;
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function radius()
    {
        return $this->radius;
    }

    /**
     * @param Integer $radius
     */
    public function changeRadius(Integer $radius)
    {
        $this->radius = $radius;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function amountOfRide()
    {
        return $this->amountOfRide;
    }

    /**
     * Decrement amount of ride
     */
    public function decrementAmountOfRide()
    {
        $this->amountOfRide = $this->amountOfRide()->dec();
    }

    /**
     * @return StringLiteral
     */
    public function eventName()
    {
        return $this->eventName;
    }

    /**
     * @return StringLiteral
     */
    public function venueAddress()
    {
        return $this->venueAddress;
    }

    /**
     * @return Coordinate
     */
    public function coordinate()
    {
        return $this->coordinate;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Deactivate the promo code.
     */
    public function deactivate()
    {
        $this->active = false;
    }

    /**
     * @return boolean
     */
    public function isExpired()
    {
        return null !== $this->expiredAt && new \DateTime() >= $this->expiredAt->toNative();
    }
}
