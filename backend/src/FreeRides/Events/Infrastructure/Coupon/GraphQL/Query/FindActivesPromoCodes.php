<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Coupon\GraphQL\Query;

use FreeRides\Events\Application\Coupon\Controller\PromoCodeReadController;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\PromoCodeType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;

/**
 * FindActivesPromoCodes class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindActivesPromoCodes extends AbstractField
{
    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return mixed
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var PromoCodeReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.promo_code');

        return $controller->findActivesAction();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'activesPromoCodes';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new ListType(new PromoCodeType());
    }
}
