<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Coupon\GraphQL\Mutation;

use FreeRides\Events\Application\Coupon\Controller\PromoCodeReadController;
use FreeRides\Events\Application\Coupon\Controller\PromoCodeWriteController;
use FreeRides\Events\Application\Coupon\ReadModel\PromoCode;
use FreeRides\Events\Infrastructure\Coupon\GraphQL\PromoCodeType;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * UsePromoCode class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class UsePromoCode extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('promoCodeId', new NonNullType(new StringType()))
            ->addArgument('origin', new NonNullType(new StringType()))
            ->addArgument('destination', new NonNullType(new StringType()))
        ;
    }

    /**¯
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return PromoCode
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var PromoCodeWriteController $controller */
        $controller = $info->getContainer()->get('app.write_controller.promo_code');
        $controller->takeARideAction(
            $args['promoCodeId'],
            $args['origin'],
            $args['destination']
        );

        /** @var PromoCodeReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.promo_code');

        return $controller->findOneByIdAction($args['promoCodeId']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'usePromoCode';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new PromoCodeType();
    }
}
