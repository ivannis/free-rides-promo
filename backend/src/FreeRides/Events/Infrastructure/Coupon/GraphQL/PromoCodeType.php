<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Coupon\GraphQL;

use FreeRides\Core\Infrastructure\GraphQL\Type\CoordinateType;
use FreeRides\Events\Application\Coupon\ReadModel\PromoCode;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\BooleanType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * PromoCodeType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->id()->toNative();
                },
            ])
            ->addField('radius', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->radius()->toNative();
                },
            ])
            ->addField('amountOfRide', [
                'type' => new NonNullType(new IntType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->amountOfRide()->toNative();
                },
            ])
            ->addField('eventId', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->eventId()->toNative();
                },
            ])
            ->addField('eventName', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->eventName()->toNative();
                },
            ])
            ->addField('venueId', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->venueId()->toNative();
                },
            ])
            ->addField('venueAddress', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->venueAddress()->toNative();
                },
            ])
            ->addField('coordinate', [
                'type' => new NonNullType(new CoordinateType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->coordinate();
                },
            ])
            ->addField('isActive', [
                'type' => new NonNullType(new BooleanType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->isActive();
                },
            ])
            ->addField('isExpired', [
                'type' => new NonNullType(new BooleanType()),
                'resolve' => function (PromoCode $promoCode) {
                    return $promoCode->isExpired();
                },
            ])
            ->addField('polyline', [
                'type' => new StringType(),
                'resolve' => function (PromoCode $promoCode, array $args, ResolveInfo $info) {
                    return $info->getExecutionContext()->getRequest()->getVariable('polyline');
                }
            ])
        ;
    }
}
