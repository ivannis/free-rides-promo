<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Coupon\Service;

use Cubiche\Domain\Identity\UUID;
use FreeRides\Events\Application\Coupon\Service\PromoCodeGeneratorInterface;
use FreeRides\Events\Domain\Coupon\PromoCodeId;


/**
 * PromoCodeGenerator class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeGenerator implements PromoCodeGeneratorInterface
{
    /**
     * @return PromoCodeId
     */
    public function next()
    {
        return PromoCodeId::fromNative(str_replace('-', '', UUID::nextUUIDValue()));
    }
}
