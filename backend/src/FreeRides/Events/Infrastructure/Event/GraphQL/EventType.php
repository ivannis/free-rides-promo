<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Event\GraphQL;

use FreeRides\Core\Infrastructure\GraphQL\Type\CoordinateType;
use FreeRides\Events\Application\Event\ReadModel\Event;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * EventType class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventType extends AbstractObjectType
{
    /**
     * {@inheritdoc}
     */
    public function build($config)
    {
        $config
            ->addField('id', [
                'type' => new NonNullType(new IdType()),
                'resolve' => function (Event $event) {
                    return $event->id()->toNative();
                },
            ])
            ->addField('name', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Event $event) {
                    return $event->name()->toNative();
                },
            ])
            ->addField('address', [
                'type' => new NonNullType(new StringType()),
                'resolve' => function (Event $event) {
                    return $event->formattedAddress()->toNative();
                },
            ])
            ->addField('coordinate', [
                'type' => new NonNullType(new CoordinateType()),
                'resolve' => function (Event $event) {
                    return $event->coordinate();
                },
            ])
            ->addField('promoCodes', [
                'type' => new ListType(new StringType()),
                'resolve' => function (Event $event) {
                    return array_map(function (PromoCodeId $promoCodeId) {
                        return $promoCodeId->toNative();
                    }, $event->promoCodes()->toArray());
                },
            ])
        ;
    }
}
