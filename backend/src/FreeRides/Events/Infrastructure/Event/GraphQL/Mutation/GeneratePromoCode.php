<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Event\GraphQL\Mutation;

use FreeRides\Events\Application\Coupon\Controller\PromoCodeReadController;
use FreeRides\Events\Application\Event\Controller\EventReadController;
use FreeRides\Events\Application\Event\Controller\EventWriteController;
use FreeRides\Events\Application\Event\ReadModel\Event;
use FreeRides\Events\Infrastructure\Event\GraphQL\EventType;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * GeneratePromoCode class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class GeneratePromoCode extends AbstractField
{
    /**
     * {@inheritdoc}
     */
    public function build(FieldConfig $config)
    {
        $config
            ->addArgument('eventId', new NonNullType(new StringType()))
            ->addArgument('radius', new NonNullType(new IntType()))
            ->addArgument('amountOfRide', new NonNullType(new IntType()))
            ->addArgument('expiredAt', new StringType())
        ;
    }

    /**¯
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return Event
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var EventWriteController $controller */
        $controller = $info->getContainer()->get('app.write_controller.event');

        $controller->generatePromoCodeAction(
            $args['eventId'],
            $args['radius'],
            $args['amountOfRide'],
            isset($args['expiredAt']) ? $args['expiredAt'] : null
        );

        /** @var EventReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.event');

        return $controller->findOneByIdAction($args['eventId']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'generatePromoCode';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new EventType();
    }
}
