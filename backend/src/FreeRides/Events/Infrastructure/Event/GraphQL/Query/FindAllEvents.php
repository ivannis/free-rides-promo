<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Infrastructure\Event\GraphQL\Query;

use FreeRides\Events\Application\Event\Controller\EventReadController;
use FreeRides\Events\Infrastructure\Event\GraphQL\EventType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Type\ListType\ListType;

/**
 * FindAllEvents class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class FindAllEvents extends AbstractField
{
    /**
     * @param null        $value
     * @param array       $args
     * @param ResolveInfo $info
     *
     * @return mixed
     */
    public function resolve($value, array $args, ResolveInfo $info)
    {
        /** @var EventReadController $controller */
        $controller = $info->getContainer()->get('app.read_controller.event');

        return $controller->findAllAction();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return new ListType(new EventType());
    }
}
