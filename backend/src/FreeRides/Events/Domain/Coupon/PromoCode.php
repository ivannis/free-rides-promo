<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon;

use Cubiche\Domain\EventSourcing\AggregateRoot;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeRadiusWasUpdated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasCreated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasDeactivated;
use FreeRides\Events\Domain\Coupon\Event\PromoCodeWasUsed;
use FreeRides\Events\Domain\Coupon\Exception\InvalidPromoCodeException;
use FreeRides\Events\Domain\Event\EventId;

/**
 * PromoCode class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCode extends AggregateRoot
{
    /**
     * @var EventId
     */
    protected $eventId;

    /**
     * @var \Cubiche\Domain\System\Integer
     */
    protected $radius;

    /**
     * @var \Cubiche\Domain\System\Integer
     */
    protected $amountOfRide;

    /**
     * @var boolean
     */
    protected $active;

    /**
     * @var DateTime
     */
    protected $expiredAt;

    /**
     * PromoCode constructor.
     *
     * @param PromoCodeId   $promoCodeId
     * @param EventId       $eventId
     * @param Integer       $radius
     * @param Integer       $amountOfRide
     * @param DateTime|null $expiredAt
     */
    public function __construct(
        PromoCodeId $promoCodeId,
        EventId $eventId,
        Integer $radius,
        Integer $amountOfRide,
        DateTime $expiredAt = null
    ) {
        parent::__construct($promoCodeId);

        $this->recordAndApplyEvent(
            new PromoCodeWasCreated(
                $promoCodeId,
                $eventId,
                $radius,
                $amountOfRide,
                $expiredAt
            )
        );
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->id;
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function radius()
    {
        return $this->radius;
    }

    /**
     * @param Integer $radius
     */
    public function changeRadius(Integer $radius)
    {
        $this->recordAndApplyEvent(
            new PromoCodeRadiusWasUpdated($this->promoCodeId(), $radius)
        );
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function amountOfRide()
    {
        return $this->amountOfRide;
    }

    /**
     * Assert a valid promo code
     */
    protected function assert()
    {
        if ($this->isExpired()) {
            throw new InvalidPromoCodeException(
                sprintf('Coupon code %s has expired.', $this->promoCodeId()->toNative())
            );
        }

        if (!$this->isActive()) {
            throw new InvalidPromoCodeException(
                sprintf('Coupon code %s is not active.', $this->promoCodeId()->toNative())
            );
        }

        if ($this->amountOfRide()->isZero() || $this->amountOfRide()->isNegative()) {
            throw new InvalidPromoCodeException(
                sprintf('Coupon code %s sold out the number of rides.', $this->promoCodeId()->toNative())
            );
        }
    }

    /**
     * Take a ride and use the promo code
     */
    public function takeARide()
    {
        $this->assert();

        $this->recordAndApplyEvent(
            new PromoCodeWasUsed($this->promoCodeId())
        );
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Deactivate the promo code.
     */
    public function deactivate()
    {
        $this->recordAndApplyEvent(
            new PromoCodeWasDeactivated($this->promoCodeId())
        );
    }

    /**
     * @return DateTime
     */
    public function expiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * @return boolean
     */
    public function isExpired()
    {
        return null !== $this->expiredAt && new \DateTime() >= $this->expiredAt->toNative();
    }

    /**
     * @param PromoCodeWasCreated $event
     */
    protected function applyPromoCodeWasCreated(PromoCodeWasCreated $event)
    {
        $this->radius = $event->radius();
        $this->amountOfRide = $event->amountOfRide();
        $this->expiredAt = $event->expiredAt();
        $this->active = true;
    }

    /**
     * @param PromoCodeRadiusWasUpdated $event
     */
    protected function applyPromoCodeRadiusWasUpdated(PromoCodeRadiusWasUpdated $event)
    {
        $this->radius = $event->radius();
    }

    /**
     * @param PromoCodeWasDeactivated $event
     */
    protected function applyPromoCodeWasDeactivated(PromoCodeWasDeactivated $event)
    {
        $this->active = false;
    }

    /**
     * @param PromoCodeWasUsed $event
     */
    protected function applyPromoCodeWasUsed(PromoCodeWasUsed $event)
    {
        $this->amountOfRide = $this->amountOfRide()->dec();
    }
}
