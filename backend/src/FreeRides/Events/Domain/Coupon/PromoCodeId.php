<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon;

use Cubiche\Domain\Identity\StringId;

/**
 * PromoCodeId class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeId extends StringId
{
}
