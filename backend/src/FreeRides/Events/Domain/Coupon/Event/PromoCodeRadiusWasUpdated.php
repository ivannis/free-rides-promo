<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\Integer;
use FreeRides\Events\Domain\Coupon\PromoCodeId;

/**
 * PromoCodeRadiusWasUpdated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeRadiusWasUpdated extends DomainEvent
{
    /**
     * @var Integer
     */
    protected $radius;

    /**
     * PromoCodeRadiusWasUpdated constructor.
     *
     * @param PromoCodeId $promoCodeId
     * @param Integer     $radius
     */
    public function __construct(
        PromoCodeId $promoCodeId,
        Integer $radius
    ) {
        parent::__construct($promoCodeId);

        $this->radius = $radius;
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->aggregateId();
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function radius()
    {
        return $this->radius;
    }
}
