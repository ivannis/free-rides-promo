<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;

/**
 * PromoCodeWasCreated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeWasCreated extends DomainEvent
{
    /**
     * @var EventId
     */
    protected $eventId;

    /**
     * @var Integer
     */
    protected $radius;

    /**
     * @var Integer
     */
    protected $amountOfRide;

    /**
     * @var DateTime
     */
    protected $expiredAt;

    /**
     * PromoCodeWasCreated constructor.
     *
     * @param PromoCodeId   $promoCodeId
     * @param EventId       $eventId
     * @param Integer       $radius
     * @param Integer       $amountOfRide
     * @param DateTime|null $expiredAt
     */
    public function __construct(
        PromoCodeId $promoCodeId,
        EventId $eventId,
        Integer $radius,
        Integer $amountOfRide,
        DateTime $expiredAt = null
    ) {
        parent::__construct($promoCodeId);

        $this->eventId = $eventId;
        $this->radius = $radius;
        $this->amountOfRide = $amountOfRide;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->aggregateId();
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->eventId;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function radius()
    {
        return $this->radius;
    }

    /**
     * @return \Cubiche\Domain\System\Integer
     */
    public function amountOfRide()
    {
        return $this->amountOfRide;
    }

    /**
     * @return DateTime
     */
    public function expiredAt()
    {
        return $this->expiredAt;
    }
}
