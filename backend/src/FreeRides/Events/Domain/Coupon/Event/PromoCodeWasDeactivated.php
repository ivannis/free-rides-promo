<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use FreeRides\Events\Domain\Coupon\PromoCodeId;

/**
 * PromoCodeWasDeactivated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeWasDeactivated extends DomainEvent
{
    /**
     * PromoCodeWasDeactivated constructor.
     *
     * @param PromoCodeId $promoCodeId
     */
    public function __construct(PromoCodeId $promoCodeId)
    {
        parent::__construct($promoCodeId);
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->aggregateId();
    }
}
