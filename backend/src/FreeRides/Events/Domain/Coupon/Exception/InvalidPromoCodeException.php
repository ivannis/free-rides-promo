<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Coupon\Exception;

use FreeRides\Core\Domain\Exception\Exception;

/**
 * InvalidPromoCodeException class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class InvalidPromoCodeException extends Exception
{
    /**
     * @param string|null $message
     */
    public function __construct($message = null)
    {
        parent::__construct(429, $message);
    }
}
