<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Tests\Units\Event;

use Cubiche\Domain\EventSourcing\AggregateRootInterface;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\Event;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Events\Domain\Tests\Units\TestCase;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * EventTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventTests extends TestCase
{
    /**
     * @return Event
     */
    protected function createEvent()
    {
          return new Event(
              EventId::next(),
              StringLiteral::fromNative('Barcelona Mobile World Congress'),
              VenueId::next()
          );
    }

    /**
     * Test class.
     */
    public function testClass()
    {
        $this
            ->testedClass
                ->implements(AggregateRootInterface::class)
        ;
    }

    /**
     * Test Name method.
     */
    public function testName()
    {
        $this
            ->given($event = $this->createEvent())
            ->then()
                ->string($event->name()->toNative())
                    ->isEqualTo('Barcelona Mobile World Congress')
        ;
    }

    /**
     * Test venueId method.
     */
    public function testVenueId()
    {
        $this
            ->given($event = $this->createEvent())
            ->then()
                ->string($event->venueId()->toNative())
                    ->isNotEmpty()
        ;
    }

    /**
     * Test promoCodes method.
     */
    public function testPromoCodes()
    {
        $this
            ->given($event = $this->createEvent())
            ->then()
                ->integer($event->promoCodes()->count())
                    ->isEqualTo(0)
                ->and()
                ->when($event->addPromoCode(PromoCodeId::fromNative('HYTCKI7V34')))
                ->then()
                    ->integer($event->promoCodes()->count())
                        ->isEqualTo(1)
                    ->and()
                    ->when($event->removePromoCode(PromoCodeId::fromNative('HYTCKI7V34')))
                    ->then()
                        ->integer($event->promoCodes()->count())
                            ->isEqualTo(0)
        ;
    }
}
