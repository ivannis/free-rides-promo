<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Tests\Units\Event\Event;

use FreeRides\Core\Domain\Tests\Units\Event\EventTestTrait;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Events\Domain\Tests\Units\TestCase;

/**
 * EventPromoCodeWasAddedTests class.
 *
 * Generated by TestGenerator on 2018-02-23 at 10:48:23.
 */
class EventPromoCodeWasAddedTests extends TestCase
{
    use EventTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getArguments()
    {
        return array(
            EventId::next(),
            PromoCodeId::fromNative('HGYFH63G58B')
        );
    }
}
