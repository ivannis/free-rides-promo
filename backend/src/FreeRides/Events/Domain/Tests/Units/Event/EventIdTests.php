<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Tests\Units\Event;

use FreeRides\Events\Domain\Tests\Units\TestCase;
use Cubiche\Domain\Model\NativeValueObjectInterface;

/**
 * EventIdTests class.
 *
 * Generated by TestGenerator on 2018-02-23 at 10:48:23.
 */
class EventIdTests extends TestCase
{
    /**
     * Test class.
     */
    public function testClass()
    {
        $this
            ->testedClass
                ->implements(NativeValueObjectInterface::class)
        ;
    }
}
