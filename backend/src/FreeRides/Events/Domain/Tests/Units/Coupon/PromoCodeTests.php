<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Tests\Units\Coupon;

use Cubiche\Domain\EventSourcing\AggregateRootInterface;
use Cubiche\Domain\System\DateTime\DateTime;
use Cubiche\Domain\System\Integer;
use FreeRides\Events\Domain\Coupon\Exception\InvalidPromoCodeException;
use FreeRides\Events\Domain\Coupon\PromoCode;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Events\Domain\Tests\Units\TestCase;

/**
 * PromoCodeTests class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class PromoCodeTests extends TestCase
{
    /**
     * @param DateTime|null $expitedAt
     *
     * @return PromoCode
     */
    protected function createPromoCode(DateTime $expitedAt = null)
    {
          return new PromoCode(
              PromoCodeId::fromNative('DKJ5A68AG587'),
              EventId::next(),
              Integer::fromNative(15),
              Integer::fromNative(2),
              $expitedAt ? $expitedAt : DateTime::fromNative(date_create()->modify('-2 days'))
          );
    }

    /**
     * Test class.
     */
    public function testClass()
    {
        $this
            ->testedClass
                ->implements(AggregateRootInterface::class)
        ;
    }

    /**
     * Test promoCodeId method.
     */
    public function testPromoCodeId()
    {
        $this
            ->given($promoCode = $this->createPromoCode())
            ->then()
                ->string($promoCode->promoCodeId()->toNative())
                    ->isEqualTo('DKJ5A68AG587')
        ;
    }

    /**
     * Test radius method.
     */
    public function testRadius()
    {
        $this
            ->given($promoCode = $this->createPromoCode())
            ->then()
                ->integer($promoCode->radius()->toNative())
                    ->isEqualTo(15)
                ->and()
                ->when($promoCode->changeRadius(Integer::fromNative(50)))
                ->then()
                    ->integer($promoCode->radius()->toNative())
                        ->isEqualTo(50)
        ;
    }

    /**
     * Test amountOfRide method.
     */
    public function testAmountOfRide()
    {
        $this
            ->given($promoCode = $this->createPromoCode())
            ->then()
                ->integer($promoCode->amountOfRide()->toNative())
                    ->isEqualTo(2)
        ;
    }

    /**
     * Test isActive method.
     */
    public function testIsActive()
    {
        $this
            ->given($promoCode = $this->createPromoCode())
            ->then()
                ->boolean($promoCode->isActive())
                    ->isTrue()
                ->and()
                ->when($promoCode->deactivate())
                ->then()
                    ->boolean($promoCode->isActive())
                        ->isFalse()
                    ->exception(function () use ($promoCode) {
                        $promoCode->takeARide();
                    })->isInstanceOf(InvalidPromoCodeException::class)
        ;
    }

    /**
     * Test isExpired method.
     */
    public function testIsExpired()
    {
        $this
            ->given($promoCode = $this->createPromoCode())
            ->then()
                ->variable($promoCode->expiredAt())
                    ->isNotNull()
                ->boolean($promoCode->isExpired())
                    ->isTrue()
                ->exception(function () use ($promoCode) {
                    $promoCode->takeARide();
                })->isInstanceOf(InvalidPromoCodeException::class)
                ->and()
                ->when($promoCode = $this->createPromoCode(DateTime::fromNative(date_create()->modify('+1 month'))))
                ->then()
                    ->boolean($promoCode->isExpired())
                        ->isFalse()
        ;
    }

    /**
     * Test takeARide method.
     */
    public function testTakeARide()
    {
        $this
            ->given($promoCode = $this->createPromoCode(DateTime::fromNative(date_create()->modify('+1 month'))))
            ->then()
                ->integer($promoCode->amountOfRide()->toNative())
                    ->isEqualTo(2)
                ->and()
                ->when($promoCode->takeARide())
                ->then()
                    ->integer($promoCode->amountOfRide()->toNative())
                        ->isEqualTo(1)
                    ->and()
                    ->when($promoCode->takeARide())
                    ->then()
                        ->integer($promoCode->amountOfRide()->toNative())
                            ->isEqualTo(0)
                        ->exception(function () use ($promoCode) {
                            $promoCode->takeARide();
                        })->isInstanceOf(InvalidPromoCodeException::class)
        ;
    }
}
