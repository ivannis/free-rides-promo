<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Event;

use Cubiche\Domain\Identity\UUID;

/**
 * EventId class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventId extends UUID
{
}
