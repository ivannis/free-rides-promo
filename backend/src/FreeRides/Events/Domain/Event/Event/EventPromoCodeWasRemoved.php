<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Event\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\EventId;

/**
 * EventPromoCodeWasRemoved class.
 *
 * @author Ivan Suárez Jerez <ivannis.suarez@gmail.com>
 */
class EventPromoCodeWasRemoved extends DomainEvent
{
    /**
     * @var PromoCodeId
     */
    protected $promoCodeId;

    /**
     * EventPromoCodeWasRemoved constructor.
     *
     * @param EventId     $eventId
     * @param PromoCodeId $promoCodeId
     */
    public function __construct(
        EventId $eventId,
        PromoCodeId $promoCodeId
    ) {
        parent::__construct($eventId);

        $this->promoCodeId = $promoCodeId;
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->aggregateId();
    }

    /**
     * @return PromoCodeId
     */
    public function promoCodeId()
    {
        return $this->promoCodeId;
    }
}
