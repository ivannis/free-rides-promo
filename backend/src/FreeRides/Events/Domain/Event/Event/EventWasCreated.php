<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Event\Event;

use Cubiche\Domain\EventSourcing\DomainEvent;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Events\Domain\Event\EventId;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * EventWasCreated class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class EventWasCreated extends DomainEvent
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var VenueId
     */
    protected $venueId;

    /**
     * EventWasCreated constructor.
     *
     * @param EventId       $eventId
     * @param StringLiteral $name
     * @param VenueId       $venueId
     */
    public function __construct(
        EventId $eventId,
        StringLiteral $name,
        VenueId $venueId
    ) {
        parent::__construct($eventId);

        $this->name = $name;
        $this->venueId = $venueId;
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->aggregateId();
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->venueId;
    }
}
