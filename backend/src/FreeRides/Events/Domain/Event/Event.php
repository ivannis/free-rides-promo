<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FreeRides\Events\Domain\Event;

use Cubiche\Core\Collections\ArrayCollection\ArrayHashMap;
use Cubiche\Domain\EventSourcing\AggregateRoot;
use Cubiche\Domain\System\StringLiteral;
use FreeRides\Events\Domain\Coupon\PromoCodeId;
use FreeRides\Events\Domain\Event\Event\EventPromoCodeWasAdded;
use FreeRides\Events\Domain\Event\Event\EventPromoCodeWasRemoved;
use FreeRides\Events\Domain\Event\Event\EventWasCreated;
use FreeRides\Location\Domain\Venue\VenueId;

/**
 * Event class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class Event extends AggregateRoot
{
    /**
     * @var StringLiteral
     */
    protected $name;

    /**
     * @var VenueId
     */
    protected $venueId;

    /**
     * @var ArrayHashMap|PromoCodeId[]
     */
    protected $promoCodes;

    /**
     * Event constructor.
     *
     * @param EventId       $eventId
     * @param StringLiteral $name
     * @param VenueId       $venueId
     */
    public function __construct(
        EventId $eventId,
        StringLiteral $name,
        VenueId $venueId
    ) {
        parent::__construct($eventId);

        $this->recordAndApplyEvent(
            new EventWasCreated(
                $eventId,
                $name,
                $venueId
            )
        );
    }

    /**
     * @return EventId
     */
    public function eventId()
    {
        return $this->id;
    }

    /**
     * @return StringLiteral
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return VenueId
     */
    public function venueId()
    {
        return $this->venueId;
    }

    /**
     * @return ArrayHashMap|PromoCodeId[]
     */
    public function promoCodes()
    {
        return $this->promoCodes;
    }

    /**
     * @param PromoCodeId $promoCodeId
     */
    public function addPromoCode(PromoCodeId $promoCodeId)
    {
        $this->recordAndApplyEvent(
            new EventPromoCodeWasAdded($this->eventId(), $promoCodeId)
        );
    }

    /**
     * @param PromoCodeId $promoCodeId
     */
    public function removePromoCode(PromoCodeId $promoCodeId)
    {
        $this->recordAndApplyEvent(
            new EventPromoCodeWasRemoved($this->eventId(), $promoCodeId)
        );
    }

    /**
     * @param EventWasCreated $event
     */
    protected function applyEventWasCreated(EventWasCreated $event)
    {
        $this->name = $event->name();
        $this->venueId = $event->venueId();
        $this->promoCodes = new ArrayHashMap();
    }

    /**
     * @param EventPromoCodeWasAdded $event
     */
    protected function applyEventPromoCodeWasAdded(EventPromoCodeWasAdded $event)
    {
        $this->promoCodes->set($event->promoCodeId()->toNative(), $event->promoCodeId());
    }

    /**
     * @param EventPromoCodeWasRemoved $event
     */
    protected function applyEventPromoCodeWasRemoved(EventPromoCodeWasRemoved $event)
    {
        $this->promoCodes->removeAt($event->promoCodeId()->toNative());
    }
}
