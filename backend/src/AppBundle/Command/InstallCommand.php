<?php

/**
 * This file is part of the FreeRides application.
 *
 * Copyright (c) FreeRides
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command;

use AppBundle\Command\Core\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory as FakerFactory;

/**
 * InstallCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class InstallCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:install')
            ->setDescription('FreeRides install command.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Installing </info>FreeRides <info>application</info>');

        $this
            ->setupStep($input, $output)
        ;

        $output->writeln('<info>FreeRides application has been successfully installed.</info>');
    }

    /**
     * Setup each step.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function setupStep(InputInterface $input, OutputInterface $output)
    {
        $this
            ->removeDatabases($input, $output)
            ->setupSystem($input, $output)
        ;


        if ($input->getOption('env') == 'dev') {
            $this->creatingEvents($input, $output);
        }

        return $this;
    }

    /**
     * Setup the app system data.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function removeDatabases(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Removing the databases.</info>');

        $commandInput = new ArrayInput(array(
            'command' => 'cubiche:mongodb:schema-drop',
            '--env' => $input->getOption('env'),
        ));

        return $this
            ->runCommand('cubiche:mongodb:schema-drop', $commandInput, $output)
        ;
    }

    /**
     * Setup the app system data.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function setupSystem(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Setting up system.</info>');

        return $this
            ->creatingLanguages($input, $output)
        ;
    }

    /**
     * Creating the language list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingLanguages(InputInterface $input, OutputInterface $output)
    {
        $commandInput = new ArrayInput(array(
            'command' => 'app:language-create-all',
            '--env' => $input->getOption('env'),
        ));

        return $this
            ->runCommand('app:language-create-all', $commandInput, $output)
        ;
    }

    /**
     * Creating the event list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function creatingEvents(InputInterface $input, OutputInterface $output)
    {
        $faker = FakerFactory::create();

        $events = array(
            array(
                'name' => 'Barcelona Mobile World Congress',
                'venueName' => 'Fira Barcelona Montjuïc',
                'venueAddress' => 'Avinguda de la Reina Maria Cristina, 08004 Barcelona',
                'countOfPromoCodes' => $faker->numberBetween(5, 20)
            ),
            array(
                'name' => 'Primavera Sound',
                'venueName' => 'Sala Apolo',
                'venueAddress' => 'Carrer Nou de la Rambla, 113, 08004 Barcelona',
                'countOfPromoCodes' => $faker->numberBetween(5, 20)
            ),
            array(
                'name' => 'Feria de Abril',
                'venueName' => 'Parc del Forum',
                'venueAddress' => 'Carrer de la Pau, 12, 08930 Sant Adrià de Besòs, Barcelona',
                'countOfPromoCodes' => $faker->numberBetween(5, 20)
            )
        );

        foreach ($events as $event) {
            $commandInput = new ArrayInput(array(
                'command' => 'app:event-create',
                'name' => $event['name'],
                'venueName' => $event['venueName'],
                'venueAddress' => $event['venueAddress'],
                'countOfPromoCodes' => $event['countOfPromoCodes'],
                '--env' => $input->getOption('env'),
            ));

            $this
                ->runCommand('app:event-create', $commandInput, $output)
            ;
        }

        return $this;
    }
}
