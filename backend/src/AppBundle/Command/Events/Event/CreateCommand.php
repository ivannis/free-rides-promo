<?php

/**
 * This file is part of the Parking application.
 *
 * Copyright (c) Cubiche
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Command\Events\Event;

use AppBundle\Command\Core\Command;
use Cubiche\Core\Validator\Exception\ValidationException;
use FreeRides\Events\Application\Coupon\Controller\PromoCodeWriteController;
use FreeRides\Events\Application\Event\Controller\EventWriteController;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory as FakerFactory;

/**
 * CreateCommand class.
 *
 * @author Ivan Suarez Jerez <ivannis.suarez@gmail.com>
 */
class CreateCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:event-create')
            ->addArgument('name', InputArgument::REQUIRED, 'Event name.')
            ->addArgument('venueName', InputArgument::REQUIRED, 'Venue name.')
            ->addArgument('venueAddress', InputArgument::REQUIRED, 'Venue address.')
            ->addArgument('countOfPromoCodes', InputArgument::REQUIRED, 'Count of promo codes.')
            ->setDescription('Creates a new event.')
            ->setHelp('This command allows you to create an event...')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $faker = FakerFactory::create();

        /** @var EventWriteController $controller */
        $controller = $this->get('app.write_controller.event');

        /** @var PromoCodeWriteController $promoCodeController */
        $promoCodeController = $this->get('app.write_controller.promo_code');

        $name = $input->getArgument('name');
        $venueName = $input->getArgument('venueName');
        $venueAddress = $input->getArgument('venueAddress');
        $countOfPromoCodes = $input->getArgument('countOfPromoCodes');

        try {
            $output->writeln('<info>Creating a new event</info>');

            $eventId = $controller->createAction($name, $venueName, $venueAddress);
            for ($i = 0; $i < $countOfPromoCodes; $i++) {
                $expiratedAt = sprintf(
                    '+%s %s',
                    $faker->randomDigitNotNull,
                    $faker->randomElement(['days', 'weeks', 'months', 'years'])
                );

                $promoCodeId = $controller->generatePromoCodeAction(
                    $eventId,
                    $faker->numberBetween(2, 50),
                    $faker->numberBetween(1, 6),
                    $faker->boolean(20) ? $expiratedAt : null
                );

                if ($faker->boolean(30)) {
                    $promoCodeController->deactivateAction($promoCodeId);
                }
            }

            $output->writeln(
                '<info>A new event with name </info>"'.$name.'"<info> has been successfully created.</info>'
            );
        } catch (ValidationException $e) {
            $this->printValidationErrors($e, $output);
        }
    }
}
