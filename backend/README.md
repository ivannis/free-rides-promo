# FreeRides Backend API

The backend project contains a graphql endpoints that will be used by the [frontend](https://gitlab.com/ivannis/free-rides-promo/blob/master/frontend) application.

- http://api.freerides.local/graphql

In development mode you will have a graphiql instance to play with it:

* [http://api.freerides.local/graphiql](http://api.freerides.local/graphiql)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

Add the development domain names in the host machine

```bash
# Get your current ip and add it to the hosts file
sudo pico /etc/hosts
your.current.ip.address freerides.local www.freerides.local api.freerides.local coverage.freerides.local mongodb.freerides.local
```

### Installing

```bash
cd free-rides-promo
docker-compose up -d
docker exec -it free-rides-app /www-data-shell
# now you are inside the free-rides-app container
composer global require "hirak/prestissimo:^0.3"
cd /var/www/backend
composer install --prefer-dist -n
bin/console app:install
```

The ```app:install``` command creates the data fixtures for testing purposes. A list of events, venues and promo-codes are created

### How to use the graphql API?

You just need to access to the graphiql browser in the following URL to start playing with it. (http://api.freerides.local/graphiql)

Here some examples of queries/mutations:

**Get all promo codes**
```php
query getAllPromoCodes {
    promoCodes {
        id
        radius
        amountOfRide
        eventId
        eventName
        venueId
        venueAddress
        coordinate {
            latitude
            longitude
        }
        isActive
        isExpired
    }
}
```

**Validate a promo code**
```php
query validate($promoCodeId: String!, $origin: String!, $destination: String!) {
    promoCode: validatePromoCode(promoCodeId: $promoCodeId, origin: $origin, destination: $destination) {
        id
        radius
        amountOfRide
        eventId
        eventName
        venueId
        venueAddress
        coordinate {
            latitude
            longitude
        }
        isActive
        isExpired
        polyline
    }
}

# variables
{
  "promoCodeId": "a-valid-promo-code",
  "origin": "your-origin",
  "amountOfRide": "your-destination"
}
```

**Create a new event**
```php
mutation createEvent($name: String!, $venueName: String!, $venueAddress: String!) {
  createEvent(name: $name, venueName: $venueName, venueAddress: $venueAddress) {
      id
      name
      address
      coordinate {
        latitude
        longitude
      }
      promoCodes
  }
}

# variables
{
  "name": "Uganda Food Expo",
  "venueName": "Hotel Africana",
  "venueAddress": "Hotel Africana, Kampala, Uganda"
}
```

**Generate promo codes for a given event**
```php
mutation generatePromoCode($eventId: String!, $radius: Int!, $amountOfRide: Int!) {
  generatePromoCode(eventId: $eventId, radius: $radius, amountOfRide: $amountOfRide) {
      id
      name
      address
      coordinate {
        latitude
        longitude
      }
      promoCodes
  }
}

# variables
{
  "eventId": "a-valid-event-id",
  "radius": 30,
  "amountOfRide": 4
}
```

**Change a promo-code radius**
```php
mutation changePromoCodeRadius($promoCodeId: String!, $radius: Int!) {
  changePromoCodeRadius(promoCodeId: $promoCodeId, radius: $radius) {
      id
      radius
      amountOfRide
      eventId
      eventName
      venueId
      venueAddress
      coordinate {
        latitude
        longitude
      }
      isActive
      isExpired      
  }
}

# variables
{
  "promoCodeId": "a-valid-promo-code",
  "radius": 50
}
```

**Deativate a promo-code**
```php
mutation deactivatePromoCode($promoCodeId: String!) {
  deactivatePromoCode(promoCodeId: $promoCodeId) {
      id
      radius
      amountOfRide
      eventId
      eventName
      venueId
      venueAddress
      coordinate {
        latitude
        longitude
      }
      isActive
      isExpired      
  }
}

# variables
{
  "promoCodeId": "a-valid-promo-code"
}
```

## Tests

I used [atoum](http://atoum.org/) library to unit testing and code coverage. In the root directory, you will find a ```.quality``` file, where you can define the code quality rules and all your unit tests suite.

### Running a unit tests suite

```bash
bin/test-unit events # or system/location
# Or if you prefer the tests with colored output 
bin/atoum -d src/FreeRides/Events/Domain/ # or System/Location
```

### Running all the unit tests suite

```bash
bin/test-unit 
# Or if you prefer the tests with colored output 
bin/atoum 
```

### Running all the tests coverage

```bash
# Run the tests coverage
# Make sure the /var/www/coverage directory is world writable
bin/test-coverage
# Check it at coverage.freerides.local
```

### Running the end to end tests

```
TO DO
```

## Built With

* [Symfony](http://symfony.com/) - The base web framework
* [Cubiche](https://github.com/cubiche/cubiche) - DDD library

## Authors

* [Ivan Suarez Jerez](https://github.com/ivannis)